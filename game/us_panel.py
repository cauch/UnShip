#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

class Usp_Element(pygame.sprite.Sprite):
    def __init__(self,parent,par,i):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.parent = parent
        self.param = par
        self.name = par.get("id",repr(par))
        self.index = i
        self.image = None
        self.pos = [0,0]
        self.rect = None
        self.nr = 1 #need refresh

    def refresh(self,reason=""):
        if self.nr == 0:
            return 1
        self.nr = 0

        if self.param.get("?","") == "bkgd":
            return self.refresh_bkgd()
        if self.param.get("?","") == "bu":
            return self.refresh_bu()
        if self.param.get("?","") == "te":
            return self.refresh_te()
        if self.param.get("?","") == "li":
            return self.refresh_li()
        if self.param.get("?","") == "ns":
            return self.refresh_ns(reason)

    def refresh_bkgd(self):
        w = self.parent.width
        h = self.parent.height
        self.pos = self.parent.pos

        self.image = pygame.Surface(( w, h )).convert_alpha()
        self.image.fill((0,0,0,160))
        
        self.rect = self.image.get_rect()
        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1

    def refresh_bu(self):
        x = self.parent.pos[0]
        dx = self.param.get("dx",0)
        x += dx
        y = self.parent.pos[1]

        w = self.param.get("w",self.parent.width-dx)
        h = self.param.get("h",24)

        if self.index > 0:
            previous = self.parent.elements_obj[self.index-1]
            if previous.param.get("?","") != "bkgd":
                y = previous.pos[1]+previous.rect.height

        text = self.param.get("text","")
        if text == "":
            text = self.param.get("t","")

        size = self.param.get("size","12")

        cl = (200,200,200,100)
        if self.param.get("status",0):
            cl = (23,182,255,100)
        if self.parent.buttons_selected == self.name:
            cl = (255,154,30,100)

        self.image = pygame.Surface(( w, h )).convert_alpha()
        self.image.fill((0,0,0,0))
        pygame.draw.rect(self.image,cl,pygame.Rect(4,4,w-8,h-8))         
        cl = (255,255,255)
        image_text = self.parent.display.fonts[size].render( text, 1, cl)
        self.image.blit(image_text,(5,5))

        self.pos = [x,y]

        self.rect = self.image.get_rect()

        if self.rect.width+dx > self.parent.width:
            self.parent.width = self.rect.width+dx
            self.parent.refresh()

        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1

    def refresh_te(self):
        x = self.parent.pos[0]
        dx = self.param.get("dx",0)
        x += dx
        y = self.parent.pos[1]
        if self.index > 0:
            previous = self.parent.elements_obj[self.index-1]
            if previous.param.get("?","") != "bkgd":
                y = previous.pos[1]+previous.rect.height

        text = self.param.get("text","")
        if text == "":
            text = self.param.get("t","")

        size = self.param.get("size","12")

        cl = (255,255,255)
        if text.startswith("#R"):
            text = text[2:]
            cl = (255,150,150)            
        self.image = self.parent.display.fonts[size].render( text, 1, cl)

        self.pos = [x,y]

        self.rect = self.image.get_rect()
        if self.rect.width+dx > self.parent.width:
            self.parent.width = self.rect.width+dx
            self.parent.refresh()

        self.rect.left = int(round(self.pos[0]+2))
        self.rect.top = int(round(self.pos[1]))
        return 1

    def refresh_li(self):
        previous_height = -1
        if self.rect:
            previous_height = self.rect.height
        x = self.parent.pos[0]
        y = self.parent.pos[1]
        if self.index > 0:
            previous = self.parent.elements_obj[self.index-1]
            if previous.param.get("?","") != "bkgd":
                y = previous.pos[1]+previous.rect.height

        list_ = self.param.get("l",[])

        size = self.param.get("size","12")
        images = []
        h = 0
        w = self.param.get("w",self.parent.width)
        for li in list_:
            text = "%s: %i"%(li[0],int(li[1]))
            images.append( self.parent.display.fonts[size].render( text, 1, (255,255,255)) )
            h += images[-1].get_height()

        self.image = pygame.Surface(( w, h )).convert_alpha()
        self.image.fill((0,0,0,0))
        cy = 0
        for i in images:
            self.image.blit(i,(2,cy))
            cy += i.get_height()

        self.pos = [x,y]

        self.rect = self.image.get_rect()
        if previous_height != self.rect.height and previous_height != -1:
            self.parent.height += self.rect.height-previous_height
            self.parent.elements_obj[0].nr = 1
            for o in self.parent.elements_obj[self.index:]:
                o.nr = 1            

        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1

    def refresh_ns(self,reason=""):
        previous_height = -1
        if self.rect:
            previous_height = self.rect.height
        x = self.parent.pos[0]
        dx = self.param.get("dx",0)
        x += dx
        y = self.parent.pos[1]
        if self.index > 0:
            previous = self.parent.elements_obj[self.index-1]
            if previous.param.get("?","") != "bkgd":
                y = previous.pos[1]+previous.rect.height

        list_ = self.param.get("l",[])

        size = self.param.get("size","12")
        images = []
        h = self.param.get("h",24)
        w = self.param.get("w",self.parent.width-dx)
        li = list_
        text = "%s: %i"%(li[0],int(li[1]))

        cl = (200,200,200,100)
        at = -1
        if "_" in reason:
            at = int(reason.split("_")[-1])
        if self.parent.numsels_selected == self.name:
            cl = (255,154,30,100)
        self.param["delta"] = 0
        if at >= 0:
            if at < 0:
                pass
            elif at < 10:
                self.param["delta"] = -1000            
            elif at < 20:
                self.param["delta"] = -100
            elif at < 30:
                self.param["delta"] = -10
            elif at < 40:
                self.param["delta"] = -1
            elif at > 90:
                self.param["delta"] = 1000
            elif at > 80:
                self.param["delta"] = 100
            elif at > 70:
                self.param["delta"] = 10
            elif at > 60:
                self.param["delta"] = 1
        if self.param["delta"]:
            contained = self.param.get("contained",0)
            can_get = self.param.get("can_get",0)
            if self.param["delta"] < 0:
                self.param["delta"] = max(self.param["delta"],-1*contained)
                if self.param["delta"]:
                    text += " %i"%self.param["delta"]
            if self.param["delta"] > 0:
                self.param["delta"] = min(self.param["delta"],can_get)
                if self.param["delta"]:
                    text += " +%i"%self.param["delta"]
        
        #if self.param.get("status",0):
        #    cl = (23,182,255,100)
        #if self.parent.buttons_selected == self.name:
        #    cl = (255,154,30,100)

        cl2 = (255,255,255)
        image_text = self.parent.display.fonts[size].render( text, 1, cl2)
        w = max(image_text.get_width()+10, w)
        self.image = pygame.Surface(( w, h )).convert_alpha()
        self.image.fill((0,0,0,0))
        pygame.draw.rect(self.image,cl,pygame.Rect(4,4,w-8,h-8))
        self.image.blit(image_text,(5,5))

        self.pos = [x,y]

        self.rect = self.image.get_rect()
        if previous_height != self.rect.height and previous_height != -1:
            self.parent.height += self.rect.height-previous_height
            self.parent.elements_obj[0].nr = 1
            for o in self.parent.elements_obj[self.index:]:
                o.nr = 1            
        if self.rect.width+dx > self.parent.width:
            self.parent.width = self.rect.width+dx
            self.parent.refresh()

        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1


class Usp_Panel():
    def __init__(self,parent):
        self.parent = parent
        self.core = parent.core
        self.display = parent.core.display
        self.elements_obj = []
        self.elements_par = []
        self.layer_obj = None
        self.rect = None
        self.pos = [0,0]
        self.width = 100
        self.height = 0
        self.buttons_selected = None
        self.numsels_selected = None
        self.mouse_drag = None

    def init(self,layer_spr,layer_obj):
        Usp_Element.containers = layer_spr
        self.layer_obj = layer_obj

    def remove_elements(self):
        for l in self.elements_obj:
            self.layer_obj.remove(l)
            l.kill()
        self.elements_obj = []
        self.elements_par = []
        self.buttons_selected = None
        self.numsels_selected = None
        self.height = 0

    def add_elements(self,list_):
        for l in list_:
            self.add_element(l)

    def add_element(self,l):
        self.elements_par.append(l)
        self.create_element(l)
        self.elements_obj[0].nr = 1#refresh()

    def create_element(self,l):
        #l is a dictionnary
        #l["?"] contains the type
        #       bkgd: background
        #       bu: button
        #       te: text
        #       li: list ([['text1',number1],['text2',number2]])
        #       ns: numerical selector (['text1',number1])
        #l["id"] is an identification id for the element
        what = l.get("?","")
        if what not in ["bu","bkgd","te","li","ns"]:
            print("uh")
        o = Usp_Element(self,l,len(self.elements_obj))
        o.refresh()
        self.elements_obj.append(o)
        self.layer_obj.append(o)
        if what in ["bu","te","li","ns"]:
            self.height += o.rect.height
        if what == "bkgd":
            self.nr = 0

    def refresh(self):
        for l in self.elements_obj:
            l.nr = 1
            l.refresh()
            l.nr = 1

    def refresh_by_name(self,name,reason=""):
        for l in self.elements_obj:
            if l.name != name:
                continue
            l.nr = 1
            l.refresh(reason)
            #l.nr = 1

    def update_element(self,name,para,value):
        #print("update element",name)
        for l in self.elements_obj:
            if l.name != name:
                continue
            if l.param.get(para,None) != value:
                l.param[para] = value
                l.nr = 1
                l.refresh()
                l.nr = 1

    def mousemotion(self,event):
        rect = self.elements_obj[0].rect
        if not rect.contains(pygame.Rect(event.pos,(1,1))):
            if self.buttons_selected != None:
                bs = self.buttons_selected
                self.buttons_selected = None
                self.refresh_by_name(bs,"mouseover")
            if self.numsels_selected != None:
                bs = self.numsels_selected
                self.numsels_selected = None
                self.refresh_by_name(bs,"mouseover")
            if self.mouse_drag == None:
                return 0
        if event.buttons[0] == 1:
            if self.mouse_drag == None:
                self.mouse_drag = event.pos
            self.pos[0] += event.pos[0]-self.mouse_drag[0]
            self.pos[1] += event.pos[1]-self.mouse_drag[1]
            self.pos[0] = max(10-self.width,self.pos[0])
            self.pos[1] = max(10-self.height,self.pos[1])
            self.pos[0] = min(self.display.screen_width-10,self.pos[0])
            self.pos[1] = min(self.display.screen_height-10,self.pos[1])
            self.refresh()
            self.mouse_drag = event.pos            
        else:
            self.mouse_drag = None

        button = None
        for l in self.elements_obj:
            if l.param.get("?","") not in ["bu"]:
                continue
            if l.rect.contains(pygame.Rect(event.pos,(1,1))):
                button = l.name
                break
        if button != self.buttons_selected:
            bs = self.buttons_selected
            self.buttons_selected = button
            self.refresh_by_name(button,"mouseover")
            self.refresh_by_name(bs)

        x_pc = 0
        ns = None
        for l in self.elements_obj:
            if l.param.get("?","") not in ["ns"]:
                continue
            if l.rect.contains(pygame.Rect(event.pos,(1,1))):
                x_pc = (event.pos[0]-l.pos[0])*100/l.rect.width
                ns = l.name
                break
        if ns is not None:
            if ns != self.numsels_selected:
                ons = self.numsels_selected
                self.numsels_selected = ns
                self.refresh_by_name(ons,"mouseover")
            self.refresh_by_name(ns,"mouseover_%i"%x_pc)

        return 1

    def mousebuttonup(self,event):
        rect = self.elements_obj[0].rect
        if not rect.contains(pygame.Rect(event.pos,(1,1))):
            return 0
        if self.mouse_drag != None:
            return 1
        if self.buttons_selected is not None:
            self.parent.click_menubutton(self.buttons_selected)
        if self.numsels_selected is not None:
            self.parent.click_menubutton(self.numsels_selected)        
        return 1
