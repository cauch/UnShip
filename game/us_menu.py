#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random

class Menuitem(pygame.sprite.Sprite):
    def __init__(self,menu,options):
        """
        Sprite corresponding to an elemet of the menu
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.menu = menu
        self.options = options
        self.isontop = 0
        if options.get("?") == "bkgd":
            #background of the panel
            xb = options.get("xb",0)
            dh = menu.display.screen_height
            dw = menu.display.screen_width
            height = options.get("h",dw-120)
            y = options.get("y",(dh-height)/2)
            self.image = pygame.Surface((dw-120-xb-xb,height)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = 60+xb
            self.rect.top = y
            self.image.fill((255, 255, 255, 100))
        if options.get("?") == "text":
            self.cl = options.get("cl",(0,0,0))
            self.text = options.get("text","")
            f = options.get("f","18")
            self.image = menu.display.fonts[f].render( self.text, 1, self.cl)
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("?") == "button":
            #button
            #added to self.menu.buttons
            self.cl = options.get("cl",(0,0,0))
            e = options.get("e",1)
            self.text = options.get("text","")
            f = options.get("f","18")
            text = menu.display.fonts[f].render( self.text, 1, self.cl)
            w = options.get("s",100)
            self.image = pygame.Surface((w+1,25+1)).convert_alpha()
            self.image.fill((0,0,0,0))
            pygame.draw.line(self.image,self.cl,(0,0),(w,0))
            pygame.draw.line(self.image,self.cl,(0,25),(w,25))
            pygame.draw.line(self.image,self.cl,(0,0),(0,25))
            pygame.draw.line(self.image,self.cl,(w,0),(w,25))
            dx = (w-text.get_width())/2
            #dx = 30
            self.image.blit(text, (dx,1))
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
            if e:
                self.menu.buttons.append(self)
            self.value = options.get("value",self.text)
        if options.get("?") == "cursor":
            #cursor (black square)
            self.image = pygame.Surface((10,10)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = -100
            self.rect.top = -100
            self.image.fill((0, 0, 0))
            self.value = -1

    def refresh(self):
        if self.isontop == 0:
            self.isontop = 1
            self.menu.menu_sprite.move_to_front(self)
            return 1
        return 0

class UsMenu:
    def __init__(self,display):
        self.display = display
        self.core = display.core
        self.size = display.size

        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []

        self.buttons = []
        self.cursor = None

    def init(self):

        self.menu_sprite = pygame.sprite.LayeredUpdates()
        Menuitem.containers = self.menu_sprite

        self.set_background()
        self.display.screen.blit(self.bkgd, (0,0))

        self.display_main()

    def set_background(self):
        if self.bkgd:
            return
        
        lob = []
        bg = [self.display.images[l] for l in lob]
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        self.bkgd.fill((100,30,80))
        return
        for x in range(int(rw/bg[0].get_width())):
            for y in range(int(rh/bg[0].get_height())):
                cbg = bg[random.randint(0,len(bg)-1)]
                self.bkgd.blit(cbg, (x*cbg.get_width(),y*cbg.get_height() ))

    def check_key(self,key):
        pass

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            pass
        elif event.type == KEYDOWN:
            key = event.key
            if key == K_UP or key == K_LEFT or key == K_z or key == K_w:
                self.move_cursor(-1)
            elif key == K_DOWN or key == K_RIGHT or key == K_s:
                self.move_cursor(1)
            elif event.key == K_SPACE or event.key == K_RETURN:
                    if self.cursor and self.cursor.value != -1:
                        self.click_on_button(self.cursor.value)

    def click_on_button(self,i,silent=0):
        if silent == 0:
            self.display.play_sound("button1",0.5)
        value = self.buttons[i].value
        if value == "quit":
            self.display.core.play = 0
        elif value == "play":
            self.display.mode = "play"
            cm = self.display.module["play"]
            cm.level_name = "ch01_cs"
            if self.core.profile.lastlevel:
                cm.level_name = self.core.profile.lastlevel
            self.core.new_game()
        elif value == "practice":
            self.display.mode = "play"
            cm = self.display.module["play"]
            cm.level_name = "random"
            self.core.new_game()
        elif value == "about":
            self.display_clean()
            self.display_about()
        elif value == "scores":
            self.display_clean()
            self.display_scores()
        elif value == "help":
            self.display_clean()
            self.display_help()
        elif value == "profile":
            self.display_clean()
            self.display_profile()
        elif value.startswith("proB_"):
            self.display_clean()
            self.display_profile(int(value.split("_")[1]))
        elif value.startswith("scoB_"):
            self.display_clean()
            self.display_scores(int(value.split("_")[1]))
        elif value.startswith("pro_"):
            self.core.profile.change_profile(value.split("_")[1])
            self.display_clean()
            self.display_profile()
        elif value == "proN":
            self.core.profile.create_new()
            self.display_clean()
            self.display_profile()
        elif value == "options":
            self.display_clean()
            self.display_options()
        elif value.startswith("optB_"):
            self.display_clean()
            self.display_options(value.split("_")[1])
        elif value == "back":
            self.display_clean()
            self.display_main()
        else:
            print("TODO",value)



    def update(self):
        dirty = []

        redraw_me = 0
        for o in self.menu_obj:
            r = o.refresh()
            if r:
                redraw_me = 1
        if redraw_me and self.menu_sprite:
            self.menu_sprite.clear(self.display.screen, self.bkgd)
            dirty.append(self.menu_sprite.draw(self.display.screen))

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def set_cursor(self,i,silent=0):
        if self.cursor.value == i:
            #already in place
            return
        self.cursor.value = i
        if i >= len(self.buttons):
            #no button for this index
            self.cursor.rect.left = -100
            return
        if silent == 0:
            self.display.play_sound("button2",0.5)
        #place the sprite

        self.cursor.image = pygame.Surface((self.buttons[i].rect.width,self.buttons[i].rect.height)).convert_alpha()
        self.cursor.rect = self.cursor.image.get_rect()
        self.cursor.rect.left = self.buttons[i].rect.left
        self.cursor.rect.top = self.buttons[i].rect.top
        self.cursor.image.fill((0, 155, 255, 70))
        #self.cursor.rect.left = self.buttons[i].rect.left+6
        #self.cursor.rect.top = self.buttons[i].rect.top+(self.buttons[i].rect.height/2)-(self.cursor.rect.height/2)

    def move_cursor(self,i):
        # i: 1 going down, -1 going up
        new_value = self.cursor.value + i
        if new_value < 0:
            new_value = len(self.buttons)-1
        if new_value >= len(self.buttons):
            new_value = 0
        self.set_cursor(new_value)

    def display_clean(self):
        otk = []
        for o in self.menu_obj:
            if o.__class__.__name__ == "Menuitem":
                otk.append(o)
                o.kill()
        for o in otk:
            self.menu_obj.remove(o)
        self.buttons = []

    def display_main(self):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":250,"h":380,"y":50}))
        x = self.menu_obj[-1].rect.left+12
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"UnShip","p":[x,y],"f":"22"}))
        y += 50
        a(Menuitem(self,
                   {"?":"button","text":"play","p":[x,y],"s":150}))
        y += 40

        e,cl = 1,(0,0,0)
        a(Menuitem(self,
                   {"?":"button","text":"practice","p":[x,y],"s":150,"e":e,"cl":cl}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"scores","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"profile","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"options","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"help","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"about","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"quit","p":[x,y],"s":150}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)


    def end(self):
        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []

        self.buttons = []
        self.cursor = None

