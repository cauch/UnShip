#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

import random
from game.us_geom import *
from game.us_core import Usc_Ship,Usc_Module

class Usw_MaterialStore:
    def __init__(self,world):
        self.world = world
        colors = world.create_colors()
        self.materials = {}
        self.materials["iron"] = {}
        self.materials["iron"]["state"] = "M"
        self.materials["iron"]["density"] = 1
        self.materials["iron"]["strength"] = 1
        self.materials["iron"]["color"] = colors[0]#(180,180,180)
        self.materials["steel"] = {}
        self.materials["steel"]["state"] = "M"
        self.materials["steel"]["density"] = 1.5
        self.materials["steel"]["strength"] = 5
        self.materials["steel"]["color"] = colors[1]#(100,100,100)
        self.materials["lead"] = {}
        self.materials["lead"]["state"] = "M"
        self.materials["lead"]["density"] = 5
        self.materials["lead"]["strength"] = 1
        self.materials["lead"]["color"] = colors[2]#(100,100,180)
        self.materials["fuel"] = {}
        self.materials["fuel"]["state"] = "F"
        self.materials["fuel"]["density"] = 0.1
        self.materials["fuel"]["efficiency"] = 1
        self.materials["fuel"]["color"] = colors[3]#(200,200,100)
        self.materials["electricity"] = {}
        self.materials["electricity"]["state"] = "F"
        self.materials["electricity"]["density"] = 1
        self.materials["electricity"]["efficiency"] = 1
        self.materials["electricity"]["color"] = colors[4]#(50,50,200)

        self.sources = {}
        self.sources["iron"] = {}
        self.sources["iron"]["radius"] = [10,20]
        self.sources["iron"]["lump_maxnumber"] = [10,100]
        self.sources["iron"]["lump_size"] = 10
        self.sources["steel"] = {}
        self.sources["steel"]["radius"] = [10,20]
        self.sources["steel"]["lump_maxnumber"] = [10,100]
        self.sources["steel"]["lump_size"] = 10
        self.sources["lead"] = {}
        self.sources["lead"]["radius"] = [10,20]
        self.sources["lead"]["lump_maxnumber"] = [10,100]
        self.sources["lead"]["lump_size"] = 10
        self.sources["fuel"] = {}
        self.sources["fuel"]["radius"] = [10,20]
        self.sources["fuel"]["lump_maxnumber"] = [10,100]
        self.sources["fuel"]["lump_size"] = 10
 

    def is_state(self,mat,state):
        di = self.materials.get(mat,{})
        if di.get("state") == state:
            return 1
        return 0

    def get_density(self,name):
        m = self.materials.get(name,{})
        return m.get("density",0)

    def get_color(self,name):
        m = self.materials.get(name,{})
        return m.get("color",(0,0,0))

    def get_source(self,mat_name,par_name):
        m = self.sources.get(mat_name,{})
        return m.get(par_name,0)

class Usw_ShipStore:
    def __init__(self,core):
        self.core = core

    def default_motor(self,ship):
        m1a = Usc_Module(ship)
        m1a.what = "motor"
        m1a.name = "motor_01"
        i = 2
        while m1a.name in ship.modules.keys():
            m1a.name = "motor_%02i"%i
            i+=1
        m1a.prop["consommation"] = [["fuel",0.1]]
        m1a.prop["power"] = 1
        m1a.prop["orientation"] = 0
        m1a.pos = [0,0]
        m1a.keys_info["up"] = ["mo.action_motor([0,1])",0,0,"up"]
        # text of the fct to execute, delay, special, description
        # special: 1: stop the loop on key event
        m1a.keys_info["do"] = ["mo.action_motor([0,-1])",0,0,"down"]
        m1a.keys_info["le"] = ["mo.action_motor([-1,0])",0,0,"left"]
        m1a.keys_info["ri"] = ["mo.action_motor([1,0])",0,0,"right"]
        m1a.keys["z"] = "up"
        m1a.keys["s"] = "do"
        m1a.keys["d"] = "ri"
        m1a.keys["q"] = "le"
        return m1a

    def default(self):
        ss = []
        ss.append( Usg_Segment([-4,-4,0 ,8]) )
        ss.append( Usg_Segment([4 , -4,0 ,8]))
        ss.append( Usg_Segment([-4,-4,4,-4])  )
        #ss.append( Usg_Segment([-6,-6,6 ,-6]) )
        #ss.append( Usg_Segment([-6,-6,-6 ,6]))
        #ss.append( Usg_Segment([6,  6,6,-6])  )
        #ss.append( Usg_Segment([6,  6,-6,6])  )
        for s in ss:
            s.prop = "M:iron"
        s = Usg_Shape(ss)
        print("area",s.get_area())
        print("com",s.get_com())
        ship = Usc_Ship(self.core)
        ship.shape = s

        mn = self.default_motor(ship)
        mn.pos = [0,-1]
        ship.modules[mn.name] = mn
        mn = self.default_motor(ship)
        mn.pos = [0,2]
        mn.keys["d"] = "le"
        mn.keys["q"] = "ri"
        ship.modules[mn.name] = mn

        m2 = Usc_Module(ship)
        m2.what = "core"
        m2.name = "core"
        m2.prop["capacity"] = 1500
        m2.pos = [0,0]

        ship.modules[m2.name] = m2

        m3 = Usc_Module(ship)
        m3.what = "laser"
        m3.name = "laser_01"
        m3.prop["orientation"] = 2
        m3.prop["consommation"] = [["fuel",0.1]]
        m3.prop["radius"] = [10,400]
        m3.prop["angle"] = 45
        m3.pos = [0,1]
        m3.keys_info["grab"] = ["mo.action_grab()",0.5,0,"grab"]
        m3.keys["e"] = "grab"

        ship.modules[m3.name] = m3

        m4 = Usc_Module(ship)
        m4.what = "tank"
        m4.name = "tank_01"
        m4.prop["capacity"] = 100
        m4.pos = [0,-2]
        ship.modules[m4.name] = m4

        mn = self.default_motor(ship)
        if "modules" not in ship.content.keys():
            ship.content["modules"] = []
        ship.content["modules"].append(["core",mn])

        if "iron" not in ship.content.keys():
            ship.content["iron"] = []
        ship.content["iron"].append(["core",100])
        if "fuel" not in ship.content.keys():
            ship.content["fuel"] = []
        ship.content["fuel"].append(["core",100])
        if "steel" not in ship.content.keys():
            ship.content["steel"] = []
        ship.content["steel"].append(["core",50])

        return ship

class Usw_Source:
    def __init__(self,core,world,material=""):
        self.core = core
        self.world = world
        self.pos = [random.randint(0,world.map_size),random.randint(0,world.map_size)]
        if material == "":
            material = list(world.materialStore.sources.keys())
            material = material[random.randint(0,len(material)-1)]
        self.material = material
        self.radius = world.materialStore.get_source(material,"radius")
        if type(self.radius) == type([]):
            self.radius = random.randint(self.radius[0],self.radius[1])
        self.lump_maxnumber = world.materialStore.get_source(material,"lump_maxnumber")
        if type(self.lump_maxnumber) == type([]):
            self.lump_maxnumber = random.randint(self.lump_maxnumber[0],self.lump_maxnumber[1])
        self.lump_size = world.materialStore.get_source(material,"lump_size")
        self.lumps = []
        #nl = self.lump_maxnumber//2 + random.randint(0,self.lump_maxnumber//2)
        nl = self.lump_maxnumber#//2 + random.randint(0,self.lump_maxnumber//2)
        for i in range(nl):
            x = ((random.randint(0,100)+random.randint(0,100)+random.randint(0,100))/150.)-1
            y = ((random.randint(0,100)+random.randint(0,100)+random.randint(0,100))/150.)-1
            r = self.radius*4
            self.lumps.append([r*x,r*y])

    def repopulate(self):
        if len(self.lumps) < self.lump_maxnumber:
            x = ((random.randint(0,100)+random.randint(0,100)+random.randint(0,100))/150.)-1
            y = ((random.randint(0,100)+random.randint(0,100)+random.randint(0,100))/150.)-1
            r = self.radius*4
            self.lumps.append([r*x,r*y])            

class Usw_World:
    def __init__(self,core):
        self.core = core

        self.iteration = 0

        self.map_size = 1000000#size, in pixel of the map

        self.map_colorpoints = []
        self.map_cp_size = 100000#distance, in pixel between the colored points
        self.map_cp_colors = self.create_colors()

        self.shipStore = Usw_ShipStore(core)
        self.materialStore = Usw_MaterialStore(self)

        #1) fill colorpoints
        rs = self.map_size//self.map_cp_size
        for j1 in range(0,rs):
            self.map_colorpoints.append([])
            for j2 in range(0,rs):
                cl1 = random.randint(0,5)
                cl2 = random.randint(0,5)
                while cl1 == cl2:
                    cl2 = random.randint(0,5)
                self.map_colorpoints[-1].append([cl1,cl2])

        #2) create districts
        #every object (source, ship, ...) is associated to a district
        #when you want to display a screen, you just loop on the objects in the district

        self.districts = {}
        
        #3) create sources
        self.sources = {}
        for i in range(100):
            if i == 0:
                s = Usw_Source(core,self,"fuel")
                s.pos = [0,0]
            else:
                s = Usw_Source(core,self)
            self.sources["s%i"%i] = s
            self.district_addandsurround("s%i"%i,s.pos[0],s.pos[1])

        #4) create ships
        self.ships = {}
        for i in range(10):
            s = self.shipStore.default()
            s.position = [random.randint(0,self.map_size),random.randint(0,self.map_size)]
            if i == 0:
                s.position = [200,200]
            self.ships["v%i"%i] = s
            self.district_addandsurround("v%i"%i,s.position[0],s.position[1])


    def district_addandsurround(self,obj,x,y):
        for i1 in range(-1,2):
            for i2 in range(-1,2):
                self.district_add(obj,x+(i1*1000),y+(i2*1000))

    def district_add(self,obj,x,y):
        xom = x%self.map_size
        yom = y%self.map_size
        rx,ry = int(xom/1000),int(yom/1000)
        key = "%i,%i"%(rx,ry)
        if key not in self.districts:
            self.districts[key] = set()
        self.districts[key] |= set([obj])

    def district_getlist(self,x,y):
        result = set()
        xom = x%self.map_size
        yom = y%self.map_size
        rx,ry = int(xom/1000),int(yom/1000)
        size = self.map_size//1000
        for i1 in range(-1,2):
            for i2 in range(-1,2):
                key = "%i,%i"%((rx+i1)%size,(ry+i2)%size)
                result |= self.districts.get(key,set())
        return list(result)

    def update(self):
        self.iteration += 1
        if self.iteration%100==0:
            skl = list(self.sources.keys())
            sk = skl[random.randint(0,len(skl)-1)]
            self.sources[sk].repopulate()

    def remove_sourcelump(self,source_name,lump_pos):
        if source_name not in self.sources.keys():
            return
        if lump_pos not in self.sources[source_name].lumps:
            return
        self.sources[source_name].lumps.remove(lump_pos)

    def get_cp_color(self,x,y):
        xom = x%self.map_size
        yom = y%self.map_size
        x0 = int(xom)//self.map_cp_size
        y0 = int(yom)//self.map_cp_size
        x1 = x0+1
        y1 = y0+1
        rx = (xom-(x0*self.map_cp_size))/self.map_cp_size
        ry = (yom-(y0*self.map_cp_size))/self.map_cp_size
        while (x1*self.map_cp_size >= self.map_size):
            x1 += -1*self.map_size//self.map_cp_size
        while (y1*self.map_cp_size >= self.map_size):
            y1 += -1*self.map_size//self.map_cp_size
        cls = []
        for j in range(2):
            cl_tl = self.map_cp_colors[self.map_colorpoints[x0][y0][j]]
            cl_tr = self.map_cp_colors[self.map_colorpoints[x1][y0][j]]
            cl_bl = self.map_cp_colors[self.map_colorpoints[x0][y1][j]]
            cl_br = self.map_cp_colors[self.map_colorpoints[x1][y1][j]]
            cl_x1 = [ ((cl_tl[i]*(1-rx)) + (cl_tr[i]*rx)) for i in range(3)]
            cl_x2 = [ ((cl_bl[i]*(1-rx)) + (cl_br[i]*rx)) for i in range(3)]
            cl = [ ((cl_x1[i]*(1-ry)) + (cl_x2[i]*ry)) for i in range(3)]
            cl = [int(x) for x in cl]
            cls.append(cl)
        return cls

    def create_name(self,li=[]):
        i = 0
        if li:
            i = li[random.randint(0,len(li)-1)]
        else:
            i = random.randint(1,6)
        t = ''
        if i <= 1:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,20,6,3,1,3,7,4
            c,v = 'rtpsdfglmcvbn','aeiuo'
            c2 = ['ch','bl','cl','tr','fl','dr']
            syl1 = ['en','on','ou','in','an']
            e,e2 = ['','n','m',''],['','e','i','a','u','o']
        elif i == 2:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 5,5,5,6,3,1,3,7,4
            c,v = 'zrtpqsdfghjklmwxcvbnrtkcgjwxrtkq','eeeaauiio'
            c2 = ['zl','zr','br','cr','pr','tr','fr','st','gr']
            syl1 = ['ou','ow','aw','ew','on','un','uw','ew']
            e,e2 = ['s','z','t','r','h','x'],['ez','az','ex','es','as','eg','aw','ew','ir','er','et','ax']
        elif i == 3:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,20,6,3,1,3,6,3
            c,v = 'pzdfghjlmwvbn','aaaeeei'
            c2 = ['mn','fl','dl','gn','gl','bl','ph']
            syl1 = ['en','on','en','in','an','an','en','an','on','un']
            e,e2 = ['l','l','m',''],['im','im','im','il','il','il','il','el','ig','em']
        elif i == 4:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 6,6,12,6,5,2,5,10,1
            c,v = 'zrtpsdhjkkkklmwxcvbnwwwhhhhllllmmmmppppxxxxxzzzzzz','aaaaiiiieuuooo'
            c2 = ['ch','xh','-','-']
            syl1 = ['on','en','-n','-z']
            e,e2 = ['a','i','o','','a','i'],['eni','ena','eno','ewa']
        elif i == 5:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,10,4,5,2,3,10,1
            c,v = 'rtpsdfghjlmxcbnrtupsdflmcbnupslmcbncntmlpns','aeiuoeia'
            c2 = ['cr','tr','bl','gl','ph','ph','ph','fl','pl']
            syl1 = ['ae','ae','um','on','en','ia']
            e,e2 = ['','s','i','s','',''],['us','um','us','is','ae']
        elif i == 6:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 8,0,10,0,5,2,4,9,1
            c,v = 'bdfgklmnprstvzbdfgklmnprstvzxcjh','ooooaaaiiiuuue'
            c2 = []
            syl1 = ['wo','wa','wi','wu','wo','wa','wi']
            e,e2 = ['','n','m','','',''],['o','o','e','a','u']
        elif i == 7:
            i1,i2,i3,i4,i5,i6,i7,i8,i9 = 0,0,10,0,5,1,2,4,1
            c,v = 'abcdefghijklmnoprstuvwxyzbcdflmnprstv','abcdefghijklmnoprstuvwxyzbcdflmnprstv'
            c2 = []
            syl1 = []
            e,e2 = [''],[]
        syl = []
        for l in syl1:
            a = c[int(len(c)*random.random())]
            syl.append(a+l)
        for l in range(i1):
            e.append(c[int(len(c)*random.random())]+e2[int(len(e2)*random.random())])
        for l in range(i2):
            e.append(c2[int(len(c2)*random.random())]+e2[int(len(e2)*random.random())])
        for l in range(i3):
            syl.append(c[int(len(c)*random.random())]+v[int(len(v)*random.random())])
        for l in range(i4):
            syl.append(c2[int(len(c2)*random.random())]+v[int(len(v)*random.random())])
        for l in range(int(random.random()*i5)+i6):
            t = t+syl[int(random.random()*len(syl))]
        t = t+e[int(random.random()*len(e))]
        if (len(t) < i7) or (len(t) > i8+int(random.random()*i9)) or (t[0]=="-"): t = self.create_name(li)
        return t

    def create_colors(self):
        #this loop can be infinite ?
        colors = []
        wd = 50
        while len(colors) < 6 and wd > 0:
            norm = random.randint(350,500)
            r = [random.randint(50,250),random.randint(50,250),random.randint(50,250)]
            rf = norm*1./(r[0]+r[1]+r[2])
            r = [int(r[0]*rf),int(r[1]*rf),int(r[2]*rf)]
            if max(r) > 255:
                continue
            ok = 1
            for l in colors:
                dr = sqrt((l[0]-r[0])**2 + (l[1]-r[1])**2 + (l[2]-r[2])**2)
                if dr < 100:
                    ok = 0
            if ok:
                colors.append(r[:])
            else:
                wd += -1
        if wd <= 0:
            return self.create_colors()
        return colors
