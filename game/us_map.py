#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

from game.us_panel import Usp_Panel
import random

class UsMap:
    def __init__(self,display):
        self.display = display
        self.core = display.core

        self.bkgd_old = None
        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn

        self.menu1 = None

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None

        self.mouseevent_object = None

    def init(self):

        self.layer0_sprite = pygame.sprite.LayeredUpdates()
        self.layer1_sprite = pygame.sprite.LayeredUpdates()
        self.layer2_sprite = pygame.sprite.LayeredUpdates()
        Usp_Panel.containers = self.layer2_sprite

        self.menu1 = Usp_Panel(self)
        self.menu1.init(self.layer2_sprite,self.layer2_obj)
        self.menu1.pos = [self.display.screen_width-60-10,10]
        self.menu1.width = 60
        self.menu1.add_elements([{"?":"bkgd"},
                                 {"?":"bu","id":"back","t":"back"}])

    def set_background(self):
        if self.bkgd:
            return
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh)).convert_alpha()
        self.bkgd.fill((20,20,20))

        world = self.core.world

        factor = world.map_size*1./min(rw,rh)
        pos = self.core.ship_player.position

        if self.bkgd_old:
            self.bkgd.blit(self.bkgd_old, (0,0))
        else:
            for i in range(10000):
                x,y = random.randint(0,rw),random.randint(0,rh)
                vx,vy = (x*factor)+(rw*-0.5*factor)+pos[0],(y*factor)+(rh*-0.5*factor)+pos[1]
                cls = world.get_cp_color(vx,vy)
                cl = cls[random.randint(0,1)]
                cl = [xx/5 for xx in cl]
                pygame.draw.circle(self.bkgd,cl,(x,y),3+random.randint(0,3))
            self.bkgd_old = self.bkgd.copy()

        cl = (255,80,0,255)
        pygame.draw.circle(self.bkgd,cl,(rw//2,rh//2),3,1)

        #source
        for l in world.sources:
            p = world.sources[l].pos
            cl = world.materialStore.get_color(world.sources[l].material)
            for dx in [0,world.map_size,-1*world.map_size]:
                for dy in [0,world.map_size,-1*world.map_size]:
                    x,y = ((p[0]+dx-pos[0])*1./factor)-(rw*-0.5),((p[1]+dy-pos[1])*1./factor)-(rh*-0.5)
                    pygame.draw.rect(self.bkgd,cl,pygame.Rect(x-5,y-5,10,10),1)

        cm = self.display.module["space"]
        if cm.navigation_point:
            cl = (255,0,0)
            p = cm.navigation_point
            for dx in [0,world.map_size,-1*world.map_size]:
                for dy in [0,world.map_size,-1*world.map_size]:
                    x,y = ((p[0]+dx-pos[0])*1./factor)-(rw*-0.5),((p[1]+dy-pos[1])*1./factor)-(rh*-0.5)
                    pygame.draw.line(self.bkgd,cl,(x-5,y-5),(x+5,y+5))
                    pygame.draw.line(self.bkgd,cl,(x-5,y+5),(x+5,y-5))
            

    def click_menubutton(self,button_id):
        if button_id == "back":
            self.core.load_space()

    def mousebuttonup(self,event):
        if event.button == 3:
            cm = self.display.module["space"]
            cm.navigation_point = None
            self.bkgd = None
            self.bkgd_first = 0
        else:
            rw,rh = self.display.screen_width, self.display.screen_height
            world = self.core.world
            factor = world.map_size*1./min(rw,rh)
            pos = self.core.ship_player.position
            x,y = event.pos[0],event.pos[1]
            vx,vy = (x*factor)+(rw*-0.5*factor)+pos[0],(y*factor)+(rh*-0.5*factor)+pos[1]
            cm = self.display.module["space"]
            cm.navigation_point = [vx%world.map_size,vy%world.map_size]
            self.bkgd = None
            self.bkgd_first = 0

    def mousemotion(self,event):
        return 0

    def check_key(self,key):
        return

    def check_events(self,event):
        if not self.mouseevent_object:
            self.mouseevent_object = [self.menu1,self]

        for meo in self.mouseevent_object[:]:
            if not meo:
                self.mouseevent_object = [self.menu1,self]
        if self.menu1 not in self.mouseevent_object:
            self.mouseevent_object = [self.menu1,self]
            

        if event.type == ACTIVEEVENT:
            ##turn on pause if the window is losing focus
            #if self.core.config.pausewhenout:
            #    if event.state == 2 and event.gain == 0:
            #        self.mode = "pause"
            pass
        elif event.type == KEYDOWN:
            key = event.key
        elif event.type == MOUSEMOTION:
            for meo in self.mouseevent_object[:]:
                block = meo.mousemotion(event)
                if block:
                    self.mouseevent_object.remove(meo)
                    self.mouseevent_object = [meo]+self.mouseevent_object
                    break
        elif event.type == MOUSEBUTTONUP and event.button < 4:
            for meo in self.mouseevent_object[:]:
                block = meo.mousebuttonup(event)
                if block:
                    break

    def update(self):
        dirty = []

        if self.bkgd == None:
            self.set_background()
            self.display.screen.blit(self.bkgd, (0,0))

        list_obj = [self.layer0_obj,self.layer1_obj,self.layer2_obj]
        list_spr = [self.layer0_sprite,self.layer1_sprite,self.layer2_sprite]
        redraw = [0,0,0]
        tokill = []

        for i in range(len(list_obj)):
            for o in list_obj[i][:]:
                r = o.refresh()
                if r==1:
                    redraw[i] = 1
                if r==-1:
                    tokill.append(o)
                    list_obj[i].remove(o)

        for i in range(len(list_obj)):
            if list_spr[i]:
                list_spr[i].clear(self.display.screen, self.bkgd)
        for i in range(len(list_obj)):
            if list_spr[i]:
                dirty.append(list_spr[i].draw(self.display.screen))

        for o in tokill:
            o.kill()

        redraw = 0

        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def end(self):
        for o in self.layer0_obj:
            o.kill()
        for o in self.layer1_obj:
            o.kill()
        for o in self.layer2_obj:
            o.kill()

        self.bkgd_old = None
        self.bkgd = None
        self.bkgd_first = 0

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None

        self.menu1 = None

        self.mouseevent_object = None


