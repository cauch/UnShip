#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
#_thread=None
#try:
#    import _thread
#except ImportError:
#    pass

from game.us_menu import UsMenu
from game.us_space import UsSpace
from game.us_hangar import UsHangar
from game.us_map import UsMap

class Display:
    def __init__(self):
        self.core = None
        
        self.mode = "menu"
        self.mode = "hangar"
        self.mode = "space"

        self.size = 20
        self.screen_width = 1200
        self.screen_height = 700
        
        self.module = {}
        self.screen_winstyle = 0
        self.screen = None
        self.clock = None
        self.music = None

        self.key_pressed = [] #list of keys currently pressed

        self.images = {}
        self.fonts = {}
        self.sounds = {}
        self.sound_channel = None

    def init(self):
        #self.module["play"] = BtrPlay(self)
        self.module["menu"] = UsMenu(self)
        self.module["space"] = UsSpace(self)
        self.module["hangar"] = UsHangar(self)
        self.module["map"] = UsMap(self)

        if not pygame.image.get_extended():
            raise (SystemExit, "Sorry, extended image module required")

        pygame.init()
        if pygame.mixer and not pygame.mixer.get_init():
            print('Warning, no sound')
            #pygame.mixer = None
        
        pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 16)
        pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 1)
        pygame.display.gl_set_attribute(pygame.GL_ALPHA_SIZE, 8)
        self.set_fullscreen(0)#self.core.profile.fullscreen)

        #filename1 = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","Nunito-SemiBold.ttf")
        #self.fonts['8'] = pygame.font.Font(filename1, 8)
        self.fonts['8'] = pygame.font.Font(pygame.font.match_font("droidsansmono"), 8)
        self.fonts['12'] = pygame.font.Font(pygame.font.match_font("droidsansmono"), 12)
        self.fonts['18'] = pygame.font.Font(pygame.font.match_font("droidsansmono"), 18)
        self.fonts['22'] = pygame.font.Font(pygame.font.match_font("droidsansmono"), 22)

        self.clock = pygame.time.Clock()
        
        self.load_px()
        #pygame.display.set_icon(self.images["icon"])
        pygame.display.set_caption('UnShip')
        pygame.mouse.set_visible(1)
        try:
            _thread.start_new_thread( self.load_all_sounds, (1, ) )
        except:
            self.load_all_sounds(1)

    def load_all_sounds(self,i):
        self.load_music()
        if i:
            self.change_volume_music()
        self.load_sound()

    def load_music(self):
        return
        if self.core.profile.sound1 == 0:
            return
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..","data","music",
                                "music.ogg")
        try:
            self.music = pygame.mixer.Sound(filename)
            self.music.play(-1)
        except:
            print("Failed to play %s"%filename)

    def change_volume_music(self):
        if not self.music:
            self.load_all_sounds(0)
        if self.music:
            factor1 = 0.2*self.core.profile.sound1/6.
            self.music.set_volume(factor1)


    def load_sound(self):
        if not pygame.mixer:
            return
        self.sound_channel = None
        di = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "data","sd")
        filenames = [ os.path.join(di,f) for f in os.listdir(di) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] == ".wav" ]
        for filename in filenames:
            fn = os.path.splitext(os.path.basename(filename))[0] 
            try:
                self.sounds[fn] = pygame.mixer.Sound(filename)
            except:
                print("Failed to load %s"%filename)
        try:
            self.sound_channel = pygame.mixer.find_channel() 
        except:
            print("Failed to initialize the sound channel")

    def play_sound(self,na,volume=1,parallel=0):
        if na not in self.sounds.keys():
            return
        factor1 = self.core.profile.sound2/6.
        try:
            if parallel != 0: 
                self.sounds[na].set_volume(volume*factor1)
                pygame.mixer.find_channel().play(self.sounds[na])
            else:
                self.sounds[na].set_volume(volume*factor1)
                self.sound_channel.queue(self.sounds[na])
        except:
            print("Failed to play %s"%na)

    def set_fullscreen(self,i):
        r = (self.screen_width, self.screen_height)
        bestdepth = pygame.display.mode_ok(r, self.screen_winstyle, 32)
        if i:
            self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN, bestdepth)
        else:
            self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF)#, bestdepth)
            #self.screen = pygame.display.set_mode(r, pygame.OPENGL | pygame.DOUBLEBUF, bestdepth)

    def load_px(self):

        di = os.path.join(os.path.abspath(os.path.dirname(__file__)),"..", "data","px")
        filenames = [ os.path.join(di,f) for f in os.listdir(di) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] == ".png" ]
        for filename in filenames:
            fn = os.path.splitext(os.path.basename(filename))[0] 
            try:
                self.images["%s"%fn] = pygame.image.load(filename).convert_alpha()
            except pygame.error:
                raise (SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error()))

    def check_key(self):
        cm = self.module[self.mode]
        for key in self.key_pressed:
            cm.check_key(key)

    def mousepress(self,button,xy):
        pass
        #if button[0] == 1:
        #    print("button")

    def check_events(self):
        cm = self.module[self.mode]
        for event in pygame.event.get():
            if event.type == QUIT:
                self.core.play = 0
            elif event.type == KEYDOWN:
                if event.key not in self.key_pressed:
                    self.key_pressed.append(event.key)
            elif event.type == KEYUP:
                if event.key in self.key_pressed:
                    self.key_pressed.remove(event.key)
            elif event.type == MOUSEBUTTONDOWN:
                self.mousepress(pygame.mouse.get_pressed(),pygame.mouse.get_pos())
            cm.check_events(event)
        self.check_key()

    def redraw(self):
        cm = self.module[self.mode]
        self.screen.blit(cm.bkgd, (0,0))
        cm.bkgd_first = 0        

    def update(self):
        self.check_events()
        cm = self.module[self.mode]
        dirty,redraw = cm.update()
        for l in dirty:
            pygame.display.update(l)
        if redraw:
            pygame.display.update()
        self.clock.tick(self.core.fps)

    def end(self):
        pass
