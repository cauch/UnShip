#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

import random
import os
import math
import time

from game.us_geom import *

class Usc_Module:
    def __init__(self,ship):
        self.what = ""
        self.ship = ship
        self.pos = [0,0]
        self.name = ""
        self.prop = {}
        self.defect = {}
        self.mass_when_empty = 10
        self.keys = {}
        self.keys_info = {}

    def copy(self,ns):
        nm = Usc_Module(ns)
        nm.what = self.what
        nm.keys = self.keys.copy()
        nm.keys_info = {}
        for k in self.keys_info:
            nm.keys_info[k] = self.keys_info[k].copy()
        nm.pos = self.pos[:]
        nm.name = self.name
        nm.prop = self.prop.copy()
        nm.mass_when_empty = self.mass_when_empty
        return nm

    def action_motor(self,arg):
        ok = 1
        for c in self.prop["consommation"]:
            if not self.ship.has_enough_material(c[0],c[1]*0.1):
                ok = 0
                break

        if ok == 0:
            return
        for c in self.prop["consommation"]:
            self.ship.consume_material(c[0],c[1]*0.1)

        power = self.prop["power"]
        px = arg[0]
        py = arg[1]
        ang = self.prop.get("orientation",0)*math.pi/2
        rx = (px*math.cos(ang))+(py*math.sin(ang))
        ry = (-1*px*math.sin(ang))+(py*math.cos(ang))
        nf = [self.pos[:],[rx*10*power,ry*10*power]]
        self.ship.forces.append(nf)
        return 0

    def action_grab(self):
        #check initial cost
        ok = 1
        for c in self.prop["consommation"]:
            if not self.ship.has_enough_material(c[0],c[1]):
                ok = 0
                break
        if ok == 0:
            return
        #check what is grabbed
        cm = self.ship.core.display.module["space"]
        r,v = cm.action_grab_find()
        #check if it's in range
        d2 = (v[0]**2)+(v[1]**2)
        if not (self.prop["radius"][0]**2 <= d2 <= self.prop["radius"][1]**2):
            return
        #check if it's the angle
        if d2:
            d = math.sqrt(d2)
            ang1 = math.atan2(v[0]/d,v[1]/d)*180/math.pi
            ang2 = [180,90,0,270][self.prop.get("orientation",0)]
            dang = ang2-ang1
            while dang > 360:
                dang += -360
            while dang < -360:
                dang += 360
            if dang > 180:
                dang = 360-dang
            if abs(dang) > self.prop["angle"]:
                return
        cm.action_grab_effect()
        q = 0
        if r:
            #check additional cost
            ok = 1
            #density = self.ship.core.world.materialStore.get_density(r.material)
            q = r.quantity#*density
            for c in self.prop["consommation"]:
                if not self.ship.has_enough_material(c[0],c[1]*q):
                    ok = 0
                break
            if ok == 0:
                r = None
        if r:
            #can we take it
            ret = self.ship.add_content("!all",r.material,r.quantity)
            if ret != "success":
                r = None
        if r:
            #pay for the grab
            for c in self.prop["consommation"]:
                self.ship.consume_material(c[0],c[1]*q)
            self.ship.core.world.remove_sourcelump(r.source_name,r.lump_pos)
            r = cm.action_grab_take(r)
        else:
            for c in self.prop["consommation"]:
                self.ship.consume_material(c[0],c[1])


class Usc_Ship:
    def __init__(self,core):
        self.core = core
        self.shape = None
        self.modules = {}
        self.content = {}
        self.forces = [] #those are the new forces in the ship frame
        self.momentum = [0,0] #this is the current momentum in the space frame
        self.com = [0,0] #this is the center of mass in the ship frame
        self.orientation = 0
        self.angular_moment = 0
        self.reduced_mass = None
        self.position = [0,0]
        self.key_delay = {}

    def copy(self):
        ns = Usc_Ship(self.core)
        ns.shape = self.shape.copy()
        for mo in self.modules:
            ns.modules[mo] = self.modules[mo].copy(ns)
        for co in self.content:
            ns.content[co] = self.content[co][:]
        ns.forces = self.forces[:]
        ns.momentum = self.momentum[:]
        ns.com = self.com[:]
        ns.orientation = self.orientation
        ns.angular_moment = self.angular_moment
        if self.reduced_mass:
            ns.reduced_mass = self.reduced_mass[:]
        ns.position = self.position[:]
        return ns

    def press_key(self,key):
        #if key in self.key_delay:
        #    de = time.time()-self.key_delay[key][0]
        #    if de > self.key_delay[key][1]:
        #        del self.key_delay[key]
        #    else:
        #        return

        for m in self.modules:
            #the order is random
            #but it's the same order as long
            #as the dico is not modified
            mo = self.modules[m]
            fct = mo.keys.get(key,None)
            if fct == None:
                continue

            list_ = mo.keys_info[fct]

            if m in self.key_delay:
                kd = self.key_delay[m]
                if key in kd:
                    de = time.time()-kd[key][0]
                    if de > kd[key][1]:
                        del kd[key]
                    else:
                        if list_[2] == 1:
                            break
                        continue

            eval(list_[0])
            if list_[1] != 0:
                if m not in self.key_delay:
                    self.key_delay[m] = {}
                self.key_delay[m][key] = [time.time(),list_[1]]
            if list_[2] == 1:
                break

    def has_enough_material(self,material,value):
        total = 0
        li = self.content.get(material,[])
        for c in li:
            #c[0]: location
            #c[1]: value
            total += c[1]
        if total >= value:
            return 1
        return 0

    def consume_material(self,material,value):
        minimum = value
        li = self.content.get(material,[])
        div = len(li)
        value_per_module = value*1./div
        #if div=0, then has_enough_material should have failed
        for c in li:
            minimum = min(c[1],value_per_module)
        
        if value_per_module > minimum:
            self.consume_material(material,minimum)
            self.consume_material(material,value_per_module-minimum)

        else:
            nli = []
            for c in li:
                nc = [c[0],c[1]-value_per_module]
                if nc[1] > 0:
                    nli.append(nc)
            self.content[material] = nli

    def remove_material(self,module_name,material,value):
        initial = self.content[material]
        final = []
        for l in initial:
            if l[0] == module_name:
                l[1] = l[1]-value
            if l[1] > 0:
                final.append(l)
        self.content[material] = initial

    def remove_stored_module(self,module_name):
        new = []
        sel_mod = None
        for m in self.content.get("modules",[]):
            if m[1].name == module_name:
                sel_mod = m[1]
            else:
                new.append(m)
        if new or self.content.get("modules",[]):
            self.content["modules"] = new
        return sel_mod

    def get_module_content(self,module_name,materials=[]):
        total = {}
        for material in self.content:
            if materials and material not in materials:
                continue
            mc = self.content[material] 
            for mmc in mc:
                if mmc[0] == module_name or module_name.startswith("!"):
                    if material not in total:
                        total[material] = 0
                        if material == "modules":
                            total[material] = []
                    if material != "modules":
                        total[material] += mmc[1]
                    else:
                        total[material].append(mmc[1])
        return total

    def get_content(self,what):
        #what is a specific module name, or a key word
        # key word are !all, !mo
        total = self.get_module_content(what)
        if what == "!mo":
            k = list(total.keys())
            for kk in k:
                if not self.core.world.materialStore.is_state(kk,"M"):
                    del total[kk]
        list_ = []
        ke = list(total.keys())
        ke.sort()
        if "modules" in ke:
            ke.remove("modules")
        for k in ke:
            list_.append([k,total[k]])
        if "modules" in total.keys():
            list_.append(["modules",len(total["modules"])])
        return list_

    def get_module_content_mass(self,module_name):
        moc = self.get_module_content(module_name)
        mass = 0
        for co in moc:
            if co == "modules":
                for imo in moc[co]:
                    mass += imo.mass_when_empty
            else:
                mass += self.core.world.materialStore.get_density(co)*moc[co]
        return mass

    def add_content_module(self,name,mod):
        mass = self.get_module_content_mass(name)
        capacity = self.modules[name].prop.get("capacity",0)
        massmod = mod.mass_when_empty
        if mass+massmod > capacity:
            print(name,mass,massmod,capacity)
            return "no space for module (%i required, %i available)"%(massmod,capacity-mass)
        if "modules" not in self.content:
            self.content["modules"] = []
        self.content["modules"].append([name,mod])
        return "success"

    def add_content(self,name,material,value):
        if material == "modules":
            newname = name
            if name == "!all":
                #find the one with largest space
                mnwc = []
                i = 0
                for m in self.modules:
                    if self.modules[m].what in ["core","tank"]:
                        i+=1
                        mass = self.get_module_content_mass(m)
                        capacity = self.modules[m].prop.get("capacity",0)
                        mnwc.append([capacity-mass,i,m])
                mnwc.sort()
                if len(mnwc) == 0:
                    newname = "core"
                else:
                    newname = mnwc[-1][2]
            return self.add_content_module(newname,value)

        los = [name]
        if name == "!all":
            los = []
            for m in self.modules:
                if self.modules[m].what not in ["core","tank"]:
                    continue
                los.append(m)

        density = self.core.world.materialStore.get_density(material)

        #remove the one that are full
        #and compute the smallest quantity such that one is full
        minimum = (value*density)+100
        space = 0
        for mn in los[:]:
            mass = self.get_module_content_mass(mn)
            capacity = self.modules[mn].prop.get("capacity",0)
            if capacity <= mass:
                los.remove(mn)
            elif minimum > capacity-mass:
                minimum = capacity-mass
            space += capacity-mass

        if len(los) == 0:
            return "all storage are full"
        
        minimum = minimum*1./density
        value_mass = density*value
        if value_mass > space:
            return "total space not enough (need %i for %s when available is %i)"%(int(value_mass),material,int(space))

        div = len(los)
        value2 = value*1./div
        if value2 > minimum:
            r1 = self.add_content(name,material,minimum*div)
            r2 = self.add_content(name,material,value-(minimum*div))
            return r2

        ocm = self.content.get(material,[])
        ncm = []
        done = []

        for vv in ocm:
            if vv[0] in los:
                nv = [vv[0],vv[1]]
                nv[1] += value2
                done.append(nv[0])
                ncm.append(nv)
            else:
                ncm.append(vv[:])

        if len(done) != len(los):
            for m in los:
                if m in done:
                    continue
                ncm.append([m,value2])
                
        self.content[material] = ncm
        return "success"

    def get_remaining_space_for_a_given_material(self,storage_name,material):
        used = self.get_module_content_mass(storage_name)
        total = self.modules[storage_name].prop.get("capacity",0)
        remaining = total-used
        density = 1
        if material != "ignore":
            density = self.core.world.materialStore.get_density(material)
        return remaining*1./density
        
    def step(self):
        self.compute_currentmotion()
        self.position[0] += self.momentum[0]
        self.position[1] += self.momentum[1]

    def rotate(self,px,py,rad):
        ax,ay = self.com[0],self.com[1]
        vx,vy = px-ax,py-ay
        #rad = self.orientation
        nx = (vx*cos(rad)) + (vy*sin(rad))
        ny = (vx*-1*sin(rad)) + (vy*cos(rad))
        return nx+ax,ny+ay

    def compute_currentmotion(self):
        masses = []
        if self.reduced_mass == None:
            for seg in self.shape.segments:
                xa = (seg.p1[0]+seg.p2[0])*0.5
                ya = (seg.p1[1]+seg.p2[1])*0.5
                le = seg.length
                material = ""
                props = seg.prop.split(",")
                for p in props:
                    if p.startswith("M:"):
                        material = p[2:]
                mass = le*self.core.world.materialStore.get_density(material)
                masses.append([xa,ya,mass])
            fs = Usg_ForceSystem([],masses)
            fs.compute_mass()
            self.reduced_mass = fs.com[:]+[fs.mass]
        masses.append(self.reduced_mass)
        for mo in self.modules:
            mass = self.modules[mo].mass_when_empty
            mass += self.get_module_content_mass(mo)
            masses.append(self.modules[mo].pos[:]+[mass])
        forces = self.forces[:]
        fs = Usg_ForceSystem(forces,masses)
        self.forces = []
        fs.update()
        #1) get the ship-frame-force, rotate it to get the space-frame-force
        fo = fs.force
        co,si = math.cos(self.orientation),math.sin(self.orientation)
        fo = [(co*fo[0])-(si*fo[1]),(si*fo[0])+(co*fo[1])]
        self.com = fs.com[:]
        #2) add this force to the current momentum
        mass = fs.mass
        self.momentum = [self.momentum[0]+(fo[0]*1.2/mass),self.momentum[1]+(fo[1]*1.2/mass)]
        #3) get the rotation and add it to the current rotation
        self.angular_moment += (0.0001*fs.angular_moment)
        self.angular_moment = 0.99*self.angular_moment
        self.orientation += self.angular_moment
        #self.angular_moment = 0

from game.us_world import Usw_World

class Core:
    def __init__(self):
        self.display = None
        self.play = 1       #0:quit, 1:play
        #self.time = 0        #time in iteration of the while loop
        self.fps = 40        #fps used in display -> 1 iteration = 1./fps seconds

        self.world = Usw_World(self)
        self.ship_player = self.world.shipStore.default()

    def load(self):
        #initialize Display
        self.display.init()

        cm = self.display.module[self.display.mode]
        cm.init()

        #main loop
        self.run()

    def load_hangar(self):
        cm = self.display.module[self.display.mode]
        cm.end()
        cm = self.display.module["hangar"]
        self.display.mode = "hangar"
        cm.init()

    def load_map(self):
        cm = self.display.module[self.display.mode]
        cm.end()
        cm = self.display.module["map"]
        self.display.mode = "map"
        cm.init()

    def load_menu(self):
        cm = self.display.module["menu"]
        self.display.mode = "menu"
        cm.init()

    def load_space(self):
        cm = self.display.module[self.display.mode]
        cm.end()
        cm = self.display.module["space"]
        self.display.mode = "space"
        cm.init()

    def run(self):
        while self.play:
            self.display.update()
            if self.display.mode != "pause":
                self.world.update()
            #    self.time += 1
            #    self.update()
        self.display.end()

    #def update(self):
    #    pass



