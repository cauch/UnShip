#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
import math
import time

from game.us_panel import Usp_Panel
from game.us_geom import Usg_Segment

class BkgdEffect(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.space = core.display.module["space"]
        self.image = pygame.Surface((80,80)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.pos = [0,0]
        self.status = []
        self.renew = 1
        self.transparency_factor = 1
        self.ontop = 0
        self.refresh()

    def reinit(self,st=0):
        self.image.fill((0,0,0,0))            
        rw,rh = self.display.screen_width, self.display.screen_height
        self.pos = [random.randint(0,rw),random.randint(0,rh)]
        self.pos[0] += (self.space.pos[0]*-10)
        self.pos[1] += (self.space.pos[1]*-10)
        #x = min(max(0,self.pos[0]*1./rw),1)
        #y = min(max(0,self.pos[1]*1./rh),1)
        #z = x*y
        #cl = [int(255*x),int(255*y),int(255*y*x)]
        pos = self.core.ship_player.position
        cls = self.core.world.get_cp_color(pos[0],pos[1])
        cl = cls[random.randint(0,1)]
        si = random.randint(6,30)
        if st==1:
            st = random.randint(0,si-5)
        self.status = [si-st,st,cl]

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.space.pause_mode != "none":
            return 0
        self.nr = 1
        if self.status == []:
            self.reinit()
        self.status[0] += -0.5
        self.status[1] += 0.5
        if self.status[0] < 0:
            if self.renew:
                self.reinit()
                return 1
            else:
                return -1

        self.pos[0] += self.space.pos[0]
        self.pos[1] += self.space.pos[1]

        size = int(self.status[1]*1.5)
        cl = self.status[2]+[min(255,int(self.status[0]*self.transparency_factor))]
        self.image = pygame.Surface((size*2,size*2)).convert_alpha()
        self.image.fill((0,0,0,0))
        pygame.draw.circle(self.image,cl,(size,size),size)

        self.rect.left = int(round(self.pos[0]))-(self.image.get_width()/2)
        self.rect.top = int(round(self.pos[1]))-(self.image.get_width()/2)
        if self.ontop==0:
            self.ontop=1
            self.space.layer0_sprite.move_to_front(self)
        return 1

class NavigatorMarker(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.space = core.display.module["space"]
        self.image = pygame.Surface((20,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.pos = [0,0]
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.space.pause_mode != "none":
            return 0
        self.nr = 1

        p = self.space.navigation_point
        if p == None:
            self.image.fill((0,0,0,0))
            return 1

        space_x = self.core.ship_player.position[0]
        space_y = self.core.ship_player.position[1]

        world = self.core.world
        best = []
        for dx in [0,world.map_size,-1*world.map_size]:
            for dy in [0,world.map_size,-1*world.map_size]:
                x,y = p[0]+dx,p[1]+dy
                best.append([(x-space_x)**2+(y-space_y)**2,x-space_x,y-space_y])
        best.sort()
        value = best[0]

        dx,dy = value[1],value[2]

        rw,rh = self.display.screen_width, self.display.screen_height
        sd = Usg_Segment([rw/2,rh/2,(rw/2)+dx,(rh/2)+dy])
        s1 = Usg_Segment([10,10,rw-20,10])
        s2 = Usg_Segment([10,10,10,rh-20])
        s3 = Usg_Segment([10,rh-20,rw-20,rh-20])
        s4 = Usg_Segment([rw-20,10,rw-20,rh-20])

        nt = [-99,-99]
        for si in [s1,s2,s3,s4]:
            r,nt = si.intersection(sd)
            if r != 0:
                break

        cl = (255,80,0,255)
        image_text = self.display.fonts["12"].render( str(int(math.sqrt(value[0])//100)), 1, cl)
        iw,ih = image_text.get_rect().width,image_text.get_rect().height

        self.image = pygame.Surface((iw+20,ih+20)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.image.blit(image_text, (10,10))

        if nt[0] != -99 and nt[0] != 0:
            self.rect.left = nt[0]-iw/2-10
            self.rect.top = nt[1]-ih/2-10+1
        return 1

class ShipSprite(pygame.sprite.Sprite):
    def __init__(self,core,ship):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.space = core.display.module["space"]
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.images = {}
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.pos = [0,0]
        rw,rh = self.display.screen_width, self.display.screen_height
        self.pos = [rw//2,rh//2]
        self.ontop = 0
        self.ship = ship
        self.player_ship = 0
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.space.pause_mode != "none":
            return 0
        self.nr = 1

        self.ship.step()
        momentum = self.ship.momentum

        if self.player_ship:
            self.space.pos[0] += -1*momentum[0]
            self.space.pos[1] += -1*momentum[1]

        shape_ori = self.ship.shape
        shape = shape_ori.copy()
        shape.rotate(self.ship.com,-1*self.ship.orientation)

        sw = shape.xmax-shape.xmin
        dx = shape.xmin
        sh = shape.ymax-shape.ymin
        dy = shape.ymin

        bo = 5
        scale = 10
        image = pygame.Surface(( scale*(sw+bo+bo), scale*(sh+bo+bo) )).convert_alpha()
        image.fill((0,0,0,0))
        for seg in shape.segments:
            x1,y1 = seg.p1[0]-dx+bo,seg.p1[1]-dy+bo 
            x2,y2 = seg.p2[0]-dx+bo,seg.p2[1]-dy+bo
            cl = (255,80,0,255)
            pygame.draw.line(image,cl,[x1*scale,y1*scale],[x2*scale,y2*scale],scale//2)
        for modn in self.ship.modules:
            mod = self.ship.modules[modn]
            x0,y0 = self.ship.rotate(mod.pos[0],mod.pos[1],-1*self.ship.orientation)
            x1,y1 = x0-dx+bo-0.5,y0-dy+bo-0.5 
            cl = (255,80,0,255)
            #pygame.draw.rect(image,cl,pygame.Rect(x1*scale,y1*scale,1*scale,1*scale),scale//2)
            pygame.draw.circle(image,cl,(int(round((x1+0.5)*scale)),int(round((y1+0.5)*scale))),int(round(scale/2)))

        display_scale = 4
        ssw = int(round( display_scale*(sw+bo+bo) ))
        ssh = int(round( display_scale*(sh+bo+bo) ))
        self.image = pygame.transform.smoothscale(image, (ssw,ssh) )

        if self.player_ship:
            self.rect.left = int(round(self.pos[0])+display_scale*(dx-bo))
            self.rect.top = int(round(self.pos[1])+display_scale*(dy-bo))
        else:
            space_x = (-1*self.core.ship_player.position[0])%self.core.world.map_size
            space_y = (-1*self.core.ship_player.position[1])%self.core.world.map_size
            x = round(self.ship.position[0]+space_x)
            y = round(self.ship.position[1]+space_y)
            while x > self.core.world.map_size//2:
                x += -1*self.core.world.map_size
            while y > self.core.world.map_size//2:
                y += -1*self.core.world.map_size
            self.rect.left = int(x)-(self.image.get_width()//2)
            self.rect.top = int(y)-(self.image.get_height()//2)

        if self.ontop==0:
            self.ontop=1
            self.space.layer2_sprite.move_to_front(self)
        return 1

class MaterialSprite(pygame.sprite.Sprite):
    def __init__(self,core,material,quantity):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.space = core.display.module["space"]
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))

        self.material = material
        self.quantity = quantity
        cl = self.core.world.materialStore.get_color(material)
        self.image_or = pygame.Surface((20,20)).convert_alpha()
        self.image_or.fill(cl)

        self.images = {}
        self.orientation = random.randint(0,360)
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.pos = [0,0]
        self.ontop = 0
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.space.pause_mode != "none":
            return 0
        if self.nr != -1:
            self.nr = 1

        space_x = (-1*self.core.ship_player.position[0])%self.core.world.map_size
        space_y = (-1*self.core.ship_player.position[1])%self.core.world.map_size

        self.orientation += 5
        if self.orientation >= 360: self.orientation += -360

        self.image = pygame.transform.rotate(self.image_or,self.orientation)
        self.image = pygame.transform.smoothscale(self.image, (self.image.get_width()//2,self.image.get_height()//2) )

        self.rect = self.image.get_rect()

        x = round(self.pos[0]+space_x)
        y = round(self.pos[1]+space_y)
        while x > self.core.world.map_size//2:
            x += -1*self.core.world.map_size
        while y > self.core.world.map_size//2:
            y += -1*self.core.world.map_size

        self.rect.left = int(x)-(self.image.get_width()//2)
        self.rect.top = int(y)-(self.image.get_height()//2)

        if self.ontop==0:
            self.ontop=1
            self.space.layer1_sprite.move_to_front(self)
        return 1


class UsSpace:
    def __init__(self,display):
        self.display = display
        self.core = display.core

        self.pause_mode = "none"

        self.navigation_point = None

        self.pos = [0,0]

        self.iteration = 0
        self.time = [None,0]

        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None
        self.layer3_obj = []
        self.layer3_sprite = None

        self.visible_object_previous = []

    def init(self):
        self.layer0_sprite = pygame.sprite.LayeredUpdates()
        self.layer1_sprite = pygame.sprite.LayeredUpdates()
        self.layer2_sprite = pygame.sprite.LayeredUpdates()
        self.layer3_sprite = pygame.sprite.LayeredUpdates()
        BkgdEffect.containers = self.layer0_sprite
        ShipSprite.containers = self.layer2_sprite
        MaterialSprite.containers = self.layer1_sprite
        NavigatorMarker.containers = self.layer2_sprite
        
        for i in range(100):
            v = BkgdEffect(self.core)
            v.reinit(1)
            self.layer0_obj.append(v)

        splayer = self.core.ship_player
        self.player = ShipSprite(self.core,splayer)
        self.player.player_ship = 1
        self.layer2_obj.append(self.player)

        nm = NavigatorMarker(self.core)
        self.layer2_obj.append(nm)

        self.menu = Usp_Panel(self)
        self.menu.init(self.layer3_sprite,self.layer3_obj)
        self.menu.pos = [self.display.screen_width-100-10,10]
        menu_list = []
        menu_list.append({"?":"bkgd"})
        menu_list.append({"?":"te","t":self.get_texttime(),"id":"time"})
        li = self.core.ship_player.get_content("!all")
        menu_list.append({"?":"li","id":"content","l":li})
        menu_list.append({"?":"bu","t":"quit","id":"quit"})
        menu_list.append({"?":"bu","t":"save","id":"save"})
        menu_list.append({"?":"bu","t":"map","id":"map"})
        menu_list.append({"?":"bu","t":"storage","id":"storage"})
        menu_list.append({"?":"bu","t":"status","id":"status"})
        menu_list.append({"?":"bu","t":"hangar","id":"hangar"})
        self.menu.add_elements(menu_list)

    def get_texttime(self):
        dt = 0
        if self.time[0] != None:
            dt = time.time()-self.time[0]
        tt = dt+self.time[1]
        ntime = int(tt)
        second = ntime % 60
        ntime = (ntime - second)/60
        minute = ntime % 60
        ntime = (ntime - minute)/60
        hour = ntime % 24
        ntime = (ntime - hour)/24
        day = ntime % 1000
        tet = "%03i/%02i:%02i:%02i"%(day,hour,minute,second)
        return tet

    def set_background(self):
        if self.bkgd:
            return
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        self.bkgd.fill((20,20,20))

    def click_menubutton(self,button_id):
        if button_id == "quit":
            self.core.play = 0
        elif button_id == "hangar":
            self.core.load_hangar()
        elif button_id == "map":
            self.core.load_map()
        else:
            print("todo",button_id)

    def check_key(self,key):
        if self.pause_mode == "none":
            print(key)
            self.core.ship_player.press_key(pygame.key.name(key))

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            ##turn on pause if the window is losing focus
            #if self.core.config.pausewhenout:
            #    if event.state == 2 and event.gain == 0:
            #        self.mode = "pause"
            pass
        elif event.type == MOUSEMOTION:
            while 1:
                block = self.menu.mousemotion(event)
                if block:
                    break
                break
        elif event.type == MOUSEBUTTONUP:
            while 1:
                block = self.menu.mousebuttonup(event)
                if block:
                    break
                break

    def action_grab_find(self):
        mouse = pygame.sprite.Sprite()
        mouse.rect = pygame.Rect(pygame.mouse.get_pos()[0],pygame.mouse.get_pos()[1],1,1)
        collided = pygame.sprite.spritecollideany(mouse,self.layer1_obj)
        vect = [-1*(pygame.mouse.get_pos()[0]-(self.display.screen_width//2)),pygame.mouse.get_pos()[1]-(self.display.screen_height//2)]
        angle = self.player.ship.orientation
        vect[0],vect[1] = (vect[0]*math.cos(angle))-(vect[1]*math.sin(angle)),(vect[0]*math.sin(angle))+(vect[1]*math.cos(angle))
        return collided,vect

    def action_grab_effect(self):
        v = BkgdEffect(self.core)
        v.pos = [pygame.mouse.get_pos()[0],pygame.mouse.get_pos()[1]]
        v.renew = 0
        v.transparency_factor = 20
        v.status = [5,4,[255,255,255]]
        self.layer0_obj.append(v)

    def action_grab_take(self,r):
        r.nr = -1
        self.layer1_obj.remove(r)
        r.kill()
        return 1

    def clear_callback(self,surf,rect):
        color = (20, 20, 20)
        surf.fill(color, rect)

    def create_new_visible_objects(self):
        splayer = self.core.ship_player.position
        list_of_things_to_display = self.core.world.district_getlist(splayer[0],splayer[1])
        for l in list_of_things_to_display:
            if l.startswith("s"):
                source = self.core.world.sources[l]
                for lump in source.lumps:
                    name = l+"_"+repr(lump)
                    if name in self.visible_object_previous:
                        continue
                    material = MaterialSprite(self.core,source.material,source.lump_size)
                    material.pos = [source.pos[0]+20*lump[0],source.pos[1]+20*lump[1]]
                    material.vop_name = name
                    material.source_name = l
                    material.lump_pos = lump[:]
                    self.layer1_obj.append(material)
                    self.visible_object_previous.append(name)
            if l.startswith("v"):
                name = l
                if name not in self.visible_object_previous:
                    ship = self.core.world.ships[l]
                    s = ShipSprite(self.core,ship)
                    self.layer2_obj.append(s)
                    self.visible_object_previous.append(name)
                    print("done",s,s.rect.left,s.rect.top)

    def update(self):
        dirty = []

        self.iteration += 1
        if self.iteration % 10 == 0:
            self.menu.update_element("time","text",self.get_texttime())
            self.menu.update_element("content","l",self.core.ship_player.get_content("!all"))

        self.create_new_visible_objects()

        if self.pause_mode == "none":
            if self.time[0] == None:
                self.time[0] = time.time()

        if self.bkgd == None:
            self.set_background()
            self.display.screen.blit(self.bkgd, (0,0))

        redraw = [0,0,0,0,0]
        lo = [self.layer0_obj,
              self.layer1_obj,
              self.layer2_obj,
              self.layer3_obj
          ]
        sp = [self.layer0_sprite,
              self.layer1_sprite,
              self.layer2_sprite,
              self.layer3_sprite
          ]
        tokill = []

        for i in range(4):
            for o in lo[i][:]:
                r = o.refresh()
                if r==1:
                    redraw[i] = 1
                if r==-1:
                    tokill.append(o)
                    lo[i].remove(o)
            if i == 0:
                self.pos = [0,0]

        for i in range(4):
            if redraw[i] and sp[i]:
                sp[i].clear(self.display.screen, self.bkgd)
        for i in range(4):
            if redraw[i] and sp[i]:
                dirty.append(sp[i].draw(self.display.screen))

        for o in tokill:
            o.kill()

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def end(self):
        for o in self.layer1_obj:
            o.kill()
        for o in self.layer2_obj:
            o.kill()

        self.bkgd = None
        self.bkgd_first = 0

        if self.time[0] != None:
            dt = time.time()-self.time[0]
        tt = dt+self.time[1]
        self.time = [None,tt]

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None
        self.layer3_obj = []
        self.layer3_sprite = None

        self.visible_object_previous = []

