#simple geometry function
#analytical computations of intersections, area, inclusions, ...
#can be replaced by a more complete / fast library ?

from math import *

def dist2(p1,p2):
    return (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2

def dist(p1,p2):
    return sqrt(dist2(p1,p2))

def vector_product(v1,v2):
    x = (v1[1]*v2[2])-(v1[2]*v2[1])
    y = (v1[2]*v2[0])-(v1[0]*v2[2])
    z = (v1[0]*v2[1])-(v1[1]*v2[0])
    return [x,y,z]

class Usg_Segment:
    def __init__(self,coord):
        """
        A segment is a line with 2 extremities.
        
        Init:
          1 parameter: coord (x1,y1,x2,y2)

        Properties:
          p1:
             (x,y) of one extremity
          p2:
             (x,y) of the other extremity
          coord:
             (x1,y1,x2,y2)
          special:
             0 if normal, 1 if vertical
          slope:
             the slope, 999 if special
          ordinate:
             the ordinate, 0 if special
          prop:
             repr of a dictionnaries, for user customization
          length:
             length

        Functions:
          change(coord),compute(),intersection_line(line),
          intersection(segment),inbox(point),swap(),
          perpendicular(point),closest_approach(point),angle(segment),
          find(x,y)
        """
        self.prop = ""
        self.change(coord)

    def __repr__(self):
        return "|=Usg_Segment:({0[0]:.3f},{0[1]:.3f})->({0[2]:.3f},{0[3]:.3f})=|".format(self.coord)

    def change(self,coord):
        """
        Reinitialize the segment with a given coord.

        Parameters:
          coord: (x1,y1,x2,y2), where (x1,y1) and (x2,y2) are the extremities

        Return:
          none
        """
        self.p1 = [coord[0],coord[1]]
        self.p2 = [coord[2],coord[3]]
        self.length = dist(self.p1,self.p2)
        self.coord = self.p1+self.p2
        self.special = 0
        self.slope = 999
        self.ordinate = 0
        self.compute()

    def compute(self):
        """
        Compute the parameters of the segment.
        To be done after each changes.
        
        Parameters:
          none

        Return:
          none
        """
        px1,py1 = self.p1[0],self.p1[1]
        px2,py2 = self.p2[0],self.p2[1]
        if px1 != px2:
            self.slope = (py1-py2)*1./(px1-px2)
            self.ordinate = py1 - (self.slope * px1)
        else:
            #vertical line
            self.special = 1
            self.slope = 999
            self.ordinate = 0

    def intersection_line(self,line):
        """
        Check if the lines defined by the segments are intersecting.
        
        Parameters:
          line: another segment

        Return:
          int: 0 if not intersecting
               1 if intersecting
               2 if overlapping
          tuple: intersection coordinate
                 [0,0] when no intersection
        """
        px1,py1 = self.p1[0],self.p1[1]
        px2,py2 = self.p2[0],self.p2[1]

        ix1,iy1 = line.p1[0],line.p1[1]
        ix2,iy2 = line.p2[0],line.p2[1]

        isv1 = self.special
        isv2 = line.special
        if isv1 and isv2:
            #parallel of vertical line
            if self.p1[0] == line.p1[0]:
                return 2,[0,0]
            else:
                return 0,[0,0]
        elif isv1:
            x = self.p1[0]
            y = line.slope * self.p1[0] + line.ordinate
            return 1,[x,y]
        elif isv2:
            x = line.p1[0]
            y = self.slope * line.p1[0] + self.ordinate
            return 1,[x,y]
        elif self.slope != line.slope:
            x = (line.ordinate-self.ordinate)/(self.slope-line.slope)
            #y = self.slope * x + self.ordinate
            y = line.slope * x + line.ordinate
            return 1,[x,y]
        else:
            #parallel
            if self.ordinate == line.ordinate:
                return 2,[0,0]
            else:
                return 0,[0,0]


    def intersection(self,segment):
        """
        Check if the two segments are intersecting.

        Parameters:
          segment: another segment

        Return:
          int: 0 if not interesecting
               1 if intersecting
               2 if intersecting with extremities
               3 if overlapping
               4 if extending
          tuple: intersection coordinate
                 [0,0] when no intersection
                 if overlap [x,y,x,y] defining the overlapping segment
        """
        r,p = self.intersection_line(segment)
        if r == 0:
            return 0,[0,0]
        elif r == 1:
            if self.inbox(p) and segment.inbox(p):
                if (p == self.p1) or (p == self.p2):
                    return 2,p
                else:
                    return 1,p
            else:
                return 0,[0,0]
        elif r == 2:
            #the only way two parallel segment can intersect without overlapping is
            #if the extremities are somehow the sames
            if segment.p1 == self.p1: return 4,self.p1
            if segment.p1 == self.p2: return 4,self.p2
            if segment.p2 == self.p1: return 4,self.p1
            if segment.p2 == self.p2: return 4,self.p2
            #then, 2 extremeties are inside
            e = []
            if segment.inbox(self.p1): e += [self.p1] if self.p1 not in e else []
            if segment.inbox(self.p2): e += [self.p2] if self.p2 not in e else []
            if self.inbox(segment.p1): e += [segment.p1] if segment.p1 not in e else []
            if self.inbox(segment.p2): e += [segment.p2] if segment.p2 not in e else []
            if not e:
                return 0,[0,0]
            if len(e) == 2:
                return 3,e[0]+e[1]
            else:
                print("Error, overlapping problem, those points : {}".format(e))
        else:
            print("Error, intersection_line returns unknown status")
            return 0,[0,0]

    def extension(self,segment):
        """
        If the two segments are parallel, check if it's extend.

        Parameters:
          segment: another segment

        Return:
          None if it's not possible
          Segment that correspond to the extension otherwise
        """
        i,n = self.intersection(segment)
        if i < 3:
            #not overlaping lines
            return None
        if i == 4:
            n = [n]
        else:
            n = [[n[0],n[1]],[n[2],n[3]]]
        ap = [segment.p1]+[segment.p2]+[self.p1]+[self.p2]
        #remove the points that are in n
        fp = [x for x in ap if x not in n]
        if len(fp) == 2:
            return Usg_Segment(fp[0]+fp[1])
        else:
            print("Error, extension finds several points: all: {}, to remove: {}, final: {}".format(ap,n,fp))
        return None
            
    def inbox(self,point):
        """
        Check if a point is in the vicinity of the segment.
        It is when it is inside the box defined by the extremities.
        
        Parameters:
          point: (x,y)

        Return:
          boolean: false if not in the vicinity
                   true if in the vicinity
        """
        px1,py1 = self.p1[0],self.p1[1]
        px2,py2 = self.p2[0],self.p2[1]
        if px1 > px2:
            px2,px1 = px1,px2
        if py1 > py2:
            py2,py1 = py1,py2

        return (px1 <= point[0] <= px2) and (py1 <= point[1] <= py2)

    def swap(self):
        """
        Swap the p1 / p2 coordinate.
        
        Parameters:
          none

        Return:
          none
        """
        self.p1,self.p2 = self.p2,self.p1
        self.coord = self.p1+self.p2

    def perpendicular(self,point):
        """
        Create the segment perpendicular to the current line defined by the segment and passing by point.
        The extremities are the intersection point with the current line and the point

        Parameters:
          point: (x,y)

        Return:
          segment
        """
        if self.special == 1:
            p1 = [point[0]+1,point[1]]
            p2 = [point[0]-1,point[1]]
        else:
            p1 = [point[0],point[1]+1]
            p2 = [point[0],point[1]-1]
            if self.slope != 0:
                nslope = -1./self.slope
                p1 = [point[0],point[1]]
                p2 = [p1[0]+1,p1[1]+nslope]
        ns = Usg_Segment(p1+p2)
        v,p = self.intersection_line(ns)
        if v != 1:
            print("Error, why the perpendicular is not intersecting ?")
        fp = p+point
        return Usg_Segment(fp)

    def closest_approach(self,point):
        """
        Find the closest approach point.
        
        Parameters:
          point: (x,y)

        Return:
          point
        """
        #1) get the perpendicular segment
        ns = self.perpendicular(point)

        #2) distance
        if self.inbox(ns.p1):
            return dist(ns.p1,point)
        else:
            return sqrt(min(dist2(self.p1,point),dist2(self.p2,point)))

    def angle(self,segment):
        """
        Get the oriented angle between self and segment
        self and segment have the same p1
        
        Parameters:
          segment

        Return:
          double (in rad)
        """

        #find the common extremity
        #swap it so it's p1 in both segment
        s1 = Usg_Segment(self.coord)
        s2 = Usg_Segment(segment.coord)
        if s1.p1 != s2.p1:
            if s1.p2 == s2.p1:
                s1.swap()
            elif s1.p1 == s2.p2:
                s2.swap()
            elif s1.p2 == s2.p2:
                s1.swap()
                s2.swap()
            else:
                print("Error, those 2 segments don't have a common point")

        #compute angle in the current frame
        a1,a2 = pi/2.,pi/2.
        if s1.p1[1] >= s1.p2[1]:
            a1 = -0.5*pi
        if s2.p1[1] >= s2.p2[1]:
            a2 = -0.5*pi
        if not s1.special:
            a1 = atan(s1.slope)
            if s1.p1[0] >= s1.p2[0]:
                if s1.slope < 0:
                    a1 = pi+a1
                else:
                    a1 = a1-pi
        if not s2.special:
            a2 = atan(s2.slope)
            if s2.p1[0] >= s2.p2[0]:
                if s2.slope < 0:
                    a2 = pi+a2
                else:
                    a2 = a2-pi
        #get the difference
        da = a1-a2
        while da > pi:
            da += -2*pi
        while da < -1*pi:
            da += 2*pi

        ##orientation
        #x1,y1,x2,y2 = s1.p2[0]-s1.p1[0],s1.p2[1]-s1.p1[1],s2.p2[0]-s2.p1[0],s2.p2[1]-s2.p1[1]
        #z = vector_product([x1,y1,0],[x2,y2,0])[2]
        #print da*180/pi,z
        return da

    def find(x=None,y=None):
        """
        From x (resp. y), get y (resp. x) for the segment
        
        Parameters:
          x,y

        Return:
          string if error
          None if not inside the segment
          double otherwise
        """
        if (x == None) and (y == None):
            return "incorrect param, both none"
        if (x != None) and (y != None):
            return "incorrect param, both filled"
        if (x != None):
            if self.special == 1:
                if x != self.p1[0]:
                    return None
                else:
                    return "manypoint"
            else:
                return self.slope * x + self.ordinate
        if (y != None):
            if self.slope == 0:
                if y != self.p1[1]:
                    return None
                else:
                    return "manypoint"
            else:
                return ( y - self.ordinate ) *1./slope
        return "uh?"

class Usg_Shape:
    def __init__(self,list):
        """
        A list of segments making (or not) a shape.
        
        Init:
          1 parameter: list of segments

        Properties:
          segments:
             the list of segment
          is_loop:
             1 if all the segments are doing a closed loop
          area:
             approximated area of the shape (None if not computed, -1 if not closed)
          area_method:
             last method used for the area
          com:
             center of mass assuming uniform density (None if not computed or not closed)
          verbose:
             list of function name for which additional info will be printed
          xmin,xmax,ymin,ymax:
             maximum and minimum values

        Functions:
          copy(),add(segment),update(),look_for_row(segment,list),inside(point),
          rotate(axispoint,radiant),resistance(segment,power),projection(line),
          closest_segment(point),
          get_area(method),surface(precision),
          get_com()
        """
        self.segments = list[:]
        self.is_loop = 0
        self.area = -1
        self.area_method = "none"
        self.verbose = []
        self.xmax,self.xmin,self.ymax,self.ymin = -1,-1,-1,-1
        self.com = None
        self.update()

    def copy(self):
        nnseg = []
        for seg in self.segments:
            nnseg.append(Usg_Segment(seg.coord))
            nnseg[-1].prop = seg.prop
        ns = Usg_Shape(nnseg)
        ns.area = self.area
        ns.area_method = self.area_method
        ns.verbose = self.verbose
        return ns

    def __repr__(self):
        return "|=Usg_Shape:"+repr([x for x in self.segments]).replace("|=","").replace("=|","")+"=|"

    def add(self,segment):
        """
        Add a new segment in the list.
        
        Parameters:
          segment: the new segment

        Return:
          "success" if success, errorcode if error
        """
        if segment.p1 == segment.p2:
            #segment with 0 lenght
            return "length=0"
        #segment overlap with another segment
        noi = []
        for s in self.segments:
            i,p = segment.intersection(s)
            if (i == 1):
                return "crossline"
            elif (i >= 3):
                ie = segment.extension(s)
                #if the segments have different purpose, don't merge them
                if s.prop != segment.prop and ie:
                    if (i == 3):
                        return "overlap"
                if s.prop == segment.prop and ie:
                    s.change(ie.coord)
                    self.update()
                    return "extended"
            if (i >= 2):
                noi.append(p)
        if len(noi) > 2 or [x for x in noi if noi.count(x) > 1]:
            return "too many connection"
        self.segments.append(segment)
        self.update()
        return "success"

    def update(self):
        """
        After a change, re-sort the segments.
        
        Parameters:
          none

        Return:
          none
        """
        if len(self.segments) < 1 or self.segments[0].p1 == self.segments[0].p2:
            self.is_loop = 0
            self.xmin,self.xmax,self.ymin,self.ymax = -1,-1,-1,-1
            self.area = -1
            self.area_method = "none"
            return
        #1) start with the first segment in the list
        cc = self.segments[:]
        cc0 = self.segments[0]
        #2) try to find loops
        cc.remove(cc[0])
        rows = []
        while 1:
            li,cc = self.look_for_row(cc0,cc)
            li = [cc0]+li
            rows.append(li)
            if not cc:
                break
            cc0 = cc[0]
            cc.remove(cc[0])
        self.is_loop = 0
        if len(rows) == 1:
            if rows[0][0].p1 == rows[0][-1].p2:
                self.is_loop = 1
        #3) sort the segment
        self.segments = sum(rows, [])

        #4) regions
        #we want a quick way to find "region"
        #an obvious way is 'int(x/L)*L,int(y/L)*L'
        #this returns for each (x,y) a id corresponding
        #to an area of L*L
        #We then have a function get_region(x,y)
        #that returns all segments that are in this region

        self.xmin = min([min(s.p1[0],s.p2[0]) for s in self.segments])
        self.xmax = max([max(s.p1[0],s.p2[0]) for s in self.segments])
        self.ymin = min([min(s.p1[1],s.p2[1]) for s in self.segments])
        self.ymax = max([max(s.p1[1],s.p2[1]) for s in self.segments])
        # from this /\, get a correct L
        #loop on segments, and for each segment, find the left point
        #get it's id
        #from the id, find the next x in the next id and try to get the y
        #do the same with y
        # -> list of points that change region

        #5) area
        if self.is_loop:
            self.area = None
        else:
            self.area = -1
        #6) comarea
        self.com = None

    def look_for_row(self,s0,lc):
        """
        Get chain of segments.
        Start on p2 of the given segment.
        If several segments are linked to a same p2, consider only the first one.
        
        Parameters:
          s0: first segment
          lc: list of remaining segments

        Return:
          li: list of linked segments
          lc: remaining segments
        """
        cc = lc[:]
        li = []
        while 1:
            r = None
            for l in cc:
                if l.p1[0] == s0.p2[0] and l.p1[1] == s0.p2[1]:
                    r = l
                    break
                if l.p2[0] == s0.p2[0] and l.p2[1] == s0.p2[1]:
                    l.swap()
                    r = l
                    break
            if r:
                s0 = r
                cc.remove(s0)
                li.append(s0)
            else:
                break
        return li,cc

    def inside(self,point):
        """
        Check if the point is inside the shape.

        Parameters:
          point: (x,y)

        Return:
          integer: 0 = outside, 2 = on the edge, 1 = inside
        """
        if not self.is_loop:
            return 0
        angle = 0
        if "inside" in self.verbose:
            print ( "Usg_Shape.inside: start looking inside, for {} segments".format(len(self.segments)) )
        for s in self.segments:
            vca = s.closest_approach(point)
            if vca < 0.001:
                if "inside" in self.verbose:
                    print ( "Usg_Shape.inside: close to a segment ! stopped there" )
                return 2

            s1 = Usg_Segment(point+s.p1)
            s2 = Usg_Segment(point+s.p2)
            a = s1.angle(s2)
            if "inside" in self.verbose:
                print ( "Usg_Shape.inside: angle between segment {} and segment {} = {}".format(s1,s2,a*180./pi) )
            angle += a
        return 1 if (abs(angle) > 0.001) else 0

    def rotate(self,axis,rad):
        """
        Rotate the shape from rad degrees around a given axis point.

        Parameters:
          axis: (x,y)
          rad: angle

        Return:
          none
        """
        ax,ay = axis[0],axis[1]
        for s in self.segments:
            vx,vy = s.p1[0]-ax,s.p1[1]-ay
            nx = (vx*cos(rad)) + (vy*sin(rad))
            ny = (vx*-1*sin(rad)) + (vy*cos(rad))
            coord = [nx+ax,ny+ay]
            vx,vy = s.p2[0]-ax,s.p2[1]-ay
            nx = (vx*cos(rad)) + (vy*sin(rad))
            ny = (vx*-1*sin(rad)) + (vy*cos(rad))
            coord += [nx+ax,ny+ay]
            s.change(coord)
        self.update()

    def resistance(self,direction,power=1):
        """
        Resistance for a given segment from p1 to p2

        Parameters:
          direction: segment, oriented from p1 to p2
          power: weight factor

        Return:
          number
        """
        list_of_intersections = []
        for s in self.segments:
            i,p = s.intersection(direction)
            if i >= 3:
                if dist2(s.p1,direction.p1) < dist2(s.p2,direction.p1):
                    p = s.p1
                else:
                    p = s.p2
            if i != 0:
                list_of_intersections.append([dist2(p,direction.p1),s,p])
        list_of_intersections.sort()
        if list_of_intersections:
            #compute the resistance
            #based on the angle
            #and on the resistance after reflexion
            s1,p = list_of_intersections[0][1],list_of_intersections[0][2]
            s2 = Usg_Segment(direction.p1+p)
            aa = Usg_Segment(p+s1.p1).angle(s2)
            ab = Usg_Segment(p+s1.p2).angle(s2)
            ra = min(abs(aa),abs(ab))
            return sin(ra)
        return -1

    def projection(self,line):
        """
        Return the segment based on the line defined by the segment, which contains the 2 extreme points of the projection

        Parameters:
          line: segment from which the line of projection is defined

        Return:
          segment corresponding to line, but limited by the projection points
        """
        ns = None
        for s in self.segments:
            #1) get the perpendicular
            ps1 = line.perpendicular(s.p1)
            ps2 = line.perpendicular(s.p2)
            if not ns:
                ns = Usg_Segment(ps1.p1+ps2.p1)
            else:
                if not ns.inbox(ps1.p1):
                    #extend ns to include ps1.p1
                    nsc = Usg_Segment(ps1.p1+ns.p2)
                    if not nsc.inbox(ns.p1):
                        nsc = Usg_Segment(ps1.p1+ns.p1)
                    ns = nsc
                if not ns.inbox(ps2.p1):
                    #extend ns to include ps2.p1
                    nsc = Usg_Segment(ps2.p1+ns.p2)
                    if not nsc.inbox(ns.p1):
                        nsc = Usg_Segment(ps2.p1+ns.p1)
                    ns = nsc
        return ns

    def closest_segment(self,point):
        """
        Return the segment closest to the point

        Parameters:
          point: the point

        Return:
          segment closest to the point
        """
        l = []
        i = 0
        for s in self.segments:
            d1 = s.closest_approach(point)
            i += 1
            l.append([d1,i,s])
        l.sort()
        if l:
            return l[0][2]
        else:
            return None


    def get_area(self,method="grid"):
        """
        Return the area, after recomputation it if needed.
        """
        if not self.area:
            self.area = self.surface()
        return self.area
    
    def surface_mc(self,precision=0.25):
        """
        Return an approximate value for the area.
        xmax,xmin,... should be defined.
        If not a loop, returns -1

        Parameters:
          precision: precsion distance for the grid

        Return:
          area
        """
        if self.is_loop == 0:
            return -1

        xmin = self.xmin
        xmax = self.xmax
        ymin = self.ymin
        ymax = self.ymax
        #1) loop on the element of the grid, based on the xmax
        ir = int((max(xmax-xmin,ymax-ymin)*1./precision)+1)
        area = 0
        for ix in range(0,ir+1):
            for iy in range(0,ir+1):
                rx,ry = xmin+(ix*precision),ymin+(iy*precision)
                #2) is it inside ?
                inside = self.inside([rx,ry])
                if inside == 2:
                    #the "inside when shifted to (0.25,0.25)" compensates the "inside when shift to (-0.25,-0.25)"
                    inside = self.inside([rx+(precision*0.25),ry+(precision*0.25)])
                if inside == 1:
                    area += precision**2
                    if "surface" in self.verbose:
                        print ( "area:{},rx:{},ry:{}".format(area,rx,ry) )
                if inside == 2:
                    area += precision**2
                    na += 1
                    if "surface" in self.verbose:
                        print ( "area:{},rx:{},ry:{},number:{}".format(area,rx,ry,na) )
        return area

    def surface(self):
        """
        Return the area of the polygon.
        If not a loop, returns -1

        Return:
          area
        """
        if self.is_loop == 0:
            return -1

        A = 0
        for i in range(len(self.segments)):
            x = self.segments[i].p1[0]
            y = self.segments[i].p1[1]
            x1 = self.segments[i].p2[0]
            y1 = self.segments[i].p2[1]
            A += (x*y1)-(x1*y)
        A /= 2
        return A

    def get_com(self):
        if self.is_loop == 0:
            return None
        if self.com:
            return self.com
        area = self.get_area()
        tot_x,tot_y = 0,0
        for i in range(len(self.segments)):
            x = self.segments[i].p1[0]
            y = self.segments[i].p1[1]
            x1 = self.segments[i].p2[0]
            y1 = self.segments[i].p2[1]
            tot_x += (x+x1)*((x*y1)-(x1*y))
            tot_y += (y+y1)*((y*x1)-(y1*x))
        tot_x /= (6*area)
        tot_y /= (6*area)
        self.com = [tot_x,tot_y]
        return self.com

class Usg_ForceSystem:
    def __init__(self,forces,masses):
        """
        A list of force, applied in a rigid object with different masses,
        resulting in motion and rotation

        A force is: [[x,y],[Fx,Fy]]
        A mass is: [x,y,m]
        
        Init:
          2 parameters: list of forces and list of masses

        Properties:
          forces:
             the list of forces
          force:
             resulting force [px,py] of the system
          angular_moment:
             intensity of the rotation in radiant
          masses:
             the list of masses and their positions
          mass:
             the resulting mass
          com:
             center of mass

        Functions:
          update(),compute_mass(),compute_force(),compute_angular()
        """
        self.forces = forces
        self.masses = masses
        self.force = [0,0]
        self.mass = 0
        self.com = [0,0]
        self.angular_moment = 0
        self.update()

    def __repr__(self):
        t = "|=Usg_ForceSystem:{}force{}".format(len(self.forces),"s" if len(self.forces)>1 else "")
        t += ",{}masse{}".format(len(self.masses),"s" if len(self.masses)>1 else "")
        t += ",F:({0[0]:.2f},{0[1]:.2f}),r:{1:.2f}".format(self.force,self.angular_moment[2])+"=|"
        return t

    def update(self):
        """
        After a change, re-compute value.
        
        Parameters:
          none

        Return:
          none
        """
        self.compute_mass()
        self.compute_force()
        self.compute_angular()

    def compute_mass(self):
        """
        Update the resulting punctial mass.

        Parameters:
          none

        Return:
          none
        """
        rm = [0,0,0]
        for l in self.masses:
            rm[0] += l[0]*l[2] 
            rm[1] += l[1]*l[2]
            rm[2] += l[2]
        if rm[2]:
            rm[0] = rm[0]*1./rm[2]
            rm[1] = rm[1]*1./rm[2]
        self.mass = rm[2]
        self.com = rm[:2]
        self.masses = [rm[:]]

    def compute_force(self):
        """
        Update the resulting force.

        Parameters:
          none

        Return:
          none
        """
        fo = [0,0]
        for l in self.forces:
            pa = l[0]
            fa = l[1]
            fo[0] += fa[0]
            fo[1] += fa[1]
        self.force = fo

    def compute_angular(self):
        """
        Update the angular moment.
        Need compute_mass before.

        Parameters:
          none

        Return:
          none
        """
        mass = self.mass
        comx,comy = self.com[0],self.com[1]
        v = 0
        for l in self.forces:
            x,y = l[0][0]-comx,l[0][1]-comy
            v+= vector_product([x,y,0],[l[1][0],l[1][1],0])[2]
        self.angular_moment = v
        
            
            
        
