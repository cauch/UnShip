#! /usr/bin/env python3

#    This file is part of UnShip.
#
#    UnShip is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    UnShip is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with UnShip.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

from game.us_panel import Usp_Panel
from game.us_geom import Usg_Segment
import math

class Ush_Segment(pygame.sprite.Sprite):
    def __init__(self,hangar,segment):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = hangar.core
        self.display = hangar.core.display
        self.hangar = hangar
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.pos = [0,0]
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.transparency = 255
        self.segment = segment
        self.status = 0
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        self.nr = 1

        scale = int(self.hangar.scale*32)
        p1 = [x*scale for x in self.segment.p1]
        p2 = [x*scale for x in self.segment.p2]
        
        xmin = min(p1[0],p2[0])
        xmax = max(p1[0],p2[0])
        ymin = min(p1[1],p2[1])
        ymax = max(p1[1],p2[1])

        dx = (xmax-xmin)
        dy = (ymax-ymin)

        bo = 5

        image = pygame.Surface(( dx+bo+bo, dy+bo+bo )).convert_alpha()
        image.fill((0,0,0,0))
        cl = (200,100,0,self.transparency)
        if self.status == 1:
            cl = (60,55,170,self.transparency)            
        if self.status == 2:
            cl = (170,15,30,self.transparency)            
        pygame.draw.line(image,cl,[bo+p1[0]-xmin,bo+p1[1]-ymin],[bo+p2[0]-xmin,bo+p2[1]-ymin],2)
        self.image = image
        self.rect = self.image.get_rect()
        
        dx = self.hangar.border
        dy = self.hangar.border
        self.pos[0] = dx - (self.hangar.position[0]*scale) + xmin - bo
        self.pos[1] = dy - (self.hangar.position[1]*scale) + ymin - bo

        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1

class Ush_Node(pygame.sprite.Sprite):
    def __init__(self,hangar,node):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = hangar.core
        self.display = hangar.core.display
        self.hangar = hangar
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.pos = [0,0]
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.node = node
        self.status = 0
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        self.nr = 1

        scale = int(self.hangar.scale*32)
        p1 = [x*scale for x in self.node]

        image = pygame.Surface(( 10, 10 )).convert_alpha()
        image.fill((0,0,0,0))
        cl = (200,100,0,255)
        if self.status == 1:
            cl = (60,55,170,255)            
        if self.status == 2:
            cl = (170,15,30,255)       
        pygame.draw.circle(image,cl,[5,5],4)
        self.image = image
        self.rect = self.image.get_rect()
        
        dx = self.hangar.border
        dy = self.hangar.border
        self.pos[0] = dx - (self.hangar.position[0]*scale) + p1[0]-5
        self.pos[1] = dy - (self.hangar.position[1]*scale) + p1[1]-5

        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1

class Ush_Module(pygame.sprite.Sprite):
    def __init__(self,hangar,module):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = hangar.core
        self.display = hangar.core.display
        self.hangar = hangar
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.pos = [0,0]
        self.transparency = 150
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.module = module
        self.status = 0
        self.indic = []
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        self.nr = 1

        scale = int(self.hangar.scale*32)
        p1 = [x*scale for x in self.module.pos]
        
        bo = 2

        image = pygame.Surface(( scale+bo+bo, scale+bo+bo )).convert_alpha()
        image.fill((0,0,0,0))
        cl = (200,100,0,self.transparency)
        if self.status == 1:
            cl = (60,55,170,self.transparency)            
        pygame.draw.circle(image,cl,[bo+(scale//2),bo+(scale//2)],scale//2)

        if self.module.prop.get("orientation",None) != None:
            ori = self.module.prop.get("orientation",0)%4
            end_pos = [bo+(scale//2),bo+(scale//2)+((ori-1)*scale//2)]
            if ori == 1 or ori == 3:
                end_pos = [bo+(scale//2)+((ori-2)*scale//2),bo+(scale//2)]
            cl = (cl[0]*0.5,cl[1]*0.5,cl[2]*0.5,self.transparency)
            pygame.draw.line(image,cl,[bo+(scale//2),bo+(scale//2)],end_pos,2)
        
        if scale >= 16 and "left" in self.indic:
            capacity = self.module.prop.get("capacity",0)
            if capacity:
                used = self.hangar.ship_c.get_module_content_mass(self.module.name)
                md = bo+(scale//2)
                d1 = (10*scale)//32 # 10
                d2 = (d1//2)+2 # 7
                d3 = (2*d1)+1 # 21
                pygame.draw.rect(image,(0,0,0),pygame.Rect(md-d1,md-d1,d2,d3),1)
                y = (d3*used)//capacity
                pygame.draw.rect(image,(0,0,0),pygame.Rect(md-d1,md-d1+d3-y,d2,y),0)
        if scale >= 16 and "right" in self.indic:
            capacity = self.module.prop.get("capacity",0)
            if capacity:
                all_mass = [self.hangar.ship_c.get_module_content_mass(_.name) for _ in self.hangar.ship_c.modules.values() if _.what in ["core","tank"]]
                used = self.hangar.ship_c.get_module_content_mass(self.module.name)
                all_mass = max(all_mass)
                md = bo+(scale//2)
                d1 = (10*scale)//32 # 10
                d2 = (d1//2)+2 # 7
                d3 = (2*d1)+1 # 21
                pygame.draw.rect(image,(0,0,0),pygame.Rect(md+d1-d2,md-d1,d2,d3),1)
                y = (d3*used)//all_mass
                pygame.draw.rect(image,(0,0,0),pygame.Rect(md+d1-d2,md-d1+d3-y,d2,y),0)

        self.image = image
        self.rect = self.image.get_rect()
        
        dx = self.hangar.border
        dy = self.hangar.border
        self.pos[0] = dx - (self.hangar.position[0]*scale) + p1[0]-scale//2-bo
        self.pos[1] = dy - (self.hangar.position[1]*scale) + p1[1]-scale//2-bo

        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1


class UsHangar:
    def __init__(self,display):
        self.display = display
        self.core = display.core

        self.edit_mode = "none"

        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn

        self.border = 0#32

        self.position = [-10,-10]
        self.scale = 1

        self.ship_o = None #original
        self.ship_l = None #last save
        self.ship_c = None #current

        self.menu1 = None
        self.menu2 = None
        self.message = None

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None

        self.selected_segment = None
        self.selected_node = None
        self.selected_material = ""
        self.selected_module = None
        self.selected_key = None

        self.storage_free = None

        self.button_drag = None
        self.mouseevent_object = None

    def init(self):

        self.layer0_sprite = pygame.sprite.LayeredUpdates()
        self.layer1_sprite = pygame.sprite.LayeredUpdates()
        self.layer2_sprite = pygame.sprite.LayeredUpdates()
        Ush_Segment.containers = self.layer0_sprite
        Ush_Node.containers = self.layer0_sprite
        Ush_Module.containers = self.layer0_sprite
        Usp_Panel.containers = self.layer1_sprite


        self.ship_o = self.core.ship_player.copy()
        self.ship_l = self.core.ship_player.copy()
        self.ship_c = self.core.ship_player.copy()
        
        self.draw_ship(1)

        self.menu1 = Usp_Panel(self)
        self.menu1.init(self.layer2_sprite,self.layer2_obj)
        self.menu1.pos = [10,10]
        self.menu1.width = 150
        self.change_menu1("main")
        self.menu2 = Usp_Panel(self)
        self.menu2.init(self.layer2_sprite,self.layer2_obj)
        self.menu2.pos = [self.display.screen_width-150-10,10]
        self.menu2.width = 150
        self.change_menu2("default")
        
        self.message = Usp_Panel(self)
        self.message.init(self.layer2_sprite,self.layer2_obj)
        self.message.pos = [10,self.display.screen_height-24]
        self.message.width = self.display.screen_width-20
        self.message.add_elements([{"?":"bkgd"},
                                   {"?":"te","id":"message","text":""}])

    def change_menu1(self,what):
        menu_list = []
        menu_list.append({"?":"bkgd"})
        self.menu1.remove_elements()
        if what == "main":
            menu_list.append({"?":"bu","t":"done","id":"done"})
            menu_list.append({"?":"bu","t":"cancel","id":"cancel"})
            menu_list.append({"?":"bu","t":"structure","id":"structure"})
            menu_list.append({"?":"bu","t":"module","id":"module"})
            menu_list.append({"?":"bu","t":"storage","id":"storage"})
        elif what == "structure":
            menu_list.append({"?":"bu","t":"save and back","id":"save_to_main"})
            menu_list.append({"?":"bu","t":"cancel and back","id":"cancel_to_main"})
            menu_list.append({"?":"bu","t":"add line","status":0,"id":"str_add_line"})
            menu_list.append({"?":"bu","t":"remove line","status":0,"id":"str_rm_line"})
        elif what == "module":
            menu_list.append({"?":"bu","t":"save and back","id":"save_to_main"})
            menu_list.append({"?":"bu","t":"cancel and back","id":"cancel_to_main"})
        elif what == "module options":
            menu_list.append({"?":"bu","t":"save and back","id":"save_to_main"})
            menu_list.append({"?":"bu","t":"cancel and back","id":"cancel_to_main"})
            if self.selected_module.module.what != "core":
                menu_list.append({"?":"bu","t":"remove","id":"module_remove"})
            if self.selected_module.module.prop.get("orientation",None) != None:
                menu_list.append({"?":"bu","t":"rotate","id":"module_rotate"})
            if self.selected_module.module.keys_info:
                menu_list.append({"?":"bu","t":"change keys","id":"module_keys"})
        elif what == "storage":
            menu_list.append({"?":"bu","t":"save and back","id":"save_to_main"})
            menu_list.append({"?":"bu","t":"cancel and back","id":"cancel_to_main"})
            menu_list.append({"?":"bu","t":"manage material","status":0,"id":"sto_mat"})
            menu_list.append({"?":"bu","t":"manage modules","status":0,"id":"sto_mod"})
        self.menu1.add_elements(menu_list)

    def change_menu2(self,what):
        self.menu2.remove_elements()
        self.menu2.width = 150
        menu_list = []
        menu_list.append({"?":"bkgd"})
        if what == "default":
            menu_list.append({"?":"te","t":"total"})
            ll = self.ship_c.get_content("!mo")
            menu_list.append({"?":"li","l":ll,"o":"mo"})
        elif what == "material selector":
            menu_list.append({"?":"te","t":"total"})
            ll = self.ship_c.get_content("!mo")
            if not self.selected_material:
                self.selected_material = ll[0][0]
            for li in ll:
                sta = 0
                if li[0] == self.selected_material:
                    sta = 1
                menu_list.append({"?":"bu",
                                  "t":"%s: %i"%(li[0],int(li[1])),
                                  "id":"mat_sel_%s"%(li[0]),
                                  "status":sta})
                if sta:
                    ms = self.core.world.materialStore.materials.get(li[0],{})
                    ks = list(ms.keys())
                    ks.sort()
                    for kk in ks:
                        menu_list.append({"?":"te","t":" %s: %s "%( kk,repr(ms[kk]) )})
        elif what == "line description":
            menu_list.append({"?":"te","t":"total"})
            ll = self.ship_c.get_content("!mo")
            menu_list.append({"?":"li","l":ll,"o":"mo"})
            menu_list.append({"?":"te","t":"line"})
            material = ""
            option = self.selected_segment.segment
            for prop in option.prop.split(","):
                if prop.startswith("M:"):
                    material = prop[2:]
            q = option.length
            menu_list.append({"?":"te","t":"%s: %i"%(material,int(q))})
        elif what == "module selector":
            menu_list.append({"?":"te","t":"modules:"})
            modules = []
            for m in self.ship_c.modules:
                modules += self.ship_c.get_module_content(m,["modules"]).get("modules",[])
            for m in modules:
                sta = 0
                if m.name == self.selected_material:
                    sta = 1
                menu_list.append({"?":"bu",
                                  "t":"%s: %s"%(m.what,m.name),
                                  "id":"mod_sel_%s"%(m.name),
                                  "status":sta})
                if sta:
                    menu_list+=self.add_module_description(m,0)
        elif what == "module description":
            menu_list.append({"?":"te","t":"module:"})
            option = self.selected_module.module
            menu_list.append({"?":"te","t":" %s (%s)"%(option.name,option.what)})
            menu_list += self.add_module_description(option,1)
        elif what == "module key selector":
            menu_list.append({"?":"te","t":"module:"})
            mod = self.selected_module.module
            menu_list.append({"?":"te","t":" %s (%s)"%(mod.name,mod.what)})
            if not mod.keys_info:
                return
            kik = list(mod.keys_info.keys())
            kik.sort()
            for kk in kik:
                keyname = "none"
                for kkk in mod.keys:
                    if mod.keys[kkk] == kk:
                        keyname = kkk
                        break
                menu_list.append({"?":"te","t":"  %s: %s "%( keyname,mod.keys_info[kk][3] )})
                sta = 0
                if self.selected_key == kk:
                    sta = 1
                menu_list.append({"?":"bu",
                                  "t":"change",
                                  "id":"mod_keyg_%s"%(kk),
                                  "status":sta})
                menu_list.append({"?":"bu",
                                  "t":"reset",
                                  "id":"mod_keyr_%s"%(kk)})
        elif what == "storagedefault":
            menu_list.append({"?":"te","t":"total"})
            ll = self.ship_c.get_content("!mo")
            menu_list.append({"?":"li","l":ll,"o":"mo"})
            menu_list += self.get_menu2_storage_free()
        elif what == "storage selector":
            per_storage = self.get_storage_perstorage()
            menu_list += self.get_menu2_storage_free()
            keys = list(per_storage.keys())
            keys.sort()
            for k in keys:
                if not per_storage[k]:
                    continue
                sta = 0
                if self.selected_module and self.selected_module.module.name == k:
                    sta = 1
                menu_list.append({"?":"bu","t":k,"id":"sto_m_%s"%k,"status":sta})
                ll = per_storage[k]
                ll.sort()
                if "modules" in [_[0] for _ in ll]:
                    n = len([_ for _ in ll if _[0] == "modules"])
                    ll = [_ for _ in ll if _[0] != "modules"]
                    menu_list.append({"?":"te","t":" %i module%s"%(n,"s" if n>1 else ""),"dx":5})
                for lll in ll:
                    dico = {"?":"ns","l":lll,"id":"ns_storage,%s,%s"%(k,lll[0]),"dx":5}
                    dico["contained"] = lll[1]
                    avail = 0
                    if self.storage_free:
                        avail = self.storage_free.get(lll[0],0)
                    space = self.ship_c.get_remaining_space_for_a_given_material(k,lll[0])
                    dico["can_get"] = min(avail,space) 
                    menu_list.append(dico)
        elif what == "storage module":
            per_storage = self.get_storage_perstorage()
            menu_list += self.get_menu2_storage_free(False)
            list_free_module = []
            if self.storage_free:
                list_free_module = self.storage_free.get("modules",[])
            for mn in list_free_module:
                menu_list.append({"?":"te","t":" "+mn.name})
            keys = list(per_storage.keys())
            keys.sort()
            for k in keys:
                mods = ([_[1:] for _ in per_storage[k] if _[0] == "modules"]+[[]])[0]
                sta = 0
                if self.selected_module and self.selected_module.module.name == k:
                    sta = 1
                menu_list.append({"?":"bu","t":k,"id":"sto_m_%s"%k,"status":sta})
                for mod in mods:
                    nm = mod.name
                    menu_list.append({"?":"bu","t":"drop %s"%nm,"id":"sto_o_%s"%nm,"dx":5})
                for mod in list_free_module:
                    nm = mod.name
                    # check if there is space first
                    remaining = self.ship_c.get_remaining_space_for_a_given_material(k,"ignore")
                    if mod.mass_when_empty < remaining:
                        menu_list.append({"?":"bu","t":"add %s"%nm,"id":"sto_p_%s|%s"%(k,nm),"dx":5})                    
        self.menu2.add_elements(menu_list)

    def get_menu2_storage_free(self, display_modules=True):
        menu_list = []
        if self.storage_free:
            menu_list.append({"?":"te","t":"free (will be lost)"})
            ll = list(self.storage_free.keys())
            ll.sort()
            ll = [[" "+_,self.storage_free[_]] for _ in ll if _ != "modules" and self.storage_free[_] > 0]
            if "modules" in self.storage_free and display_modules:
                nl = len(self.storage_free["modules"])
                if nl:
                    ll += [[" module%s"%("s"*(min(1,nl-1))),nl]]
            menu_list.append({"?":"li","l":ll})
        return menu_list

    def get_storage_perstorage(self):
        total = self.ship_c.content
        list_of_mat = list(self.ship_c.content.keys())
        per_storage = {}
        for modn in self.ship_c.modules:
            mod = self.ship_c.modules[modn]
            if mod.what not in ["core","tank"]:
                continue
            per_storage[modn] = []
            for mat in list_of_mat:
                found = [_[1] for _ in total[mat] if _[0] == modn]
                if len(found)==1:
                    per_storage[modn].append([mat, found[0]])
                elif len(found)==0:
                    if mat != "modules":
                        per_storage[modn].append([mat, 0])
                else:
                    print("perstorage error")
        print("per_storage",per_storage)
        return per_storage

    def add_module_description(self,mod,showkey):
        menu_list = []
        menu_list.append({"?":"te","t":" mass: %i"%mod.mass_when_empty})
        kp = list(mod.prop.keys())
        kp.sort()
        for kk in kp:
            menu_list.append({"?":"te","t":" %s: %s "%( kk,repr(mod.prop[kk]) )})
        if mod.keys_info and showkey:
            menu_list.append({"?":"te","t":" keys: "})
            kik = list(mod.keys_info.keys())
            kik.sort()
            for kk in kik:
                keyname = "none"
                for kkk in mod.keys:
                    if mod.keys[kkk] == kk:
                        keyname = kkk
                        break
                menu_list.append({"?":"te","t":"  %s: %s "%( keyname,mod.keys_info[kk][3] )})
        return menu_list

    def click_menubutton(self,button_id):
        self.message.update_element("message","text","")
        if self.selected_segment:
            self.layer0_obj.remove(self.selected_segment)
            self.selected_segment.kill()
        if button_id == "done":
            self.core.ship_player = self.ship_c.copy()
            self.clean_place()
            self.core.load_space()
        elif button_id == "cancel":
            self.clean_place()
            self.core.load_space()
        elif button_id == "structure":
            self.change_menu1("structure")
            self.change_menu2("default")
            self.clean_place()
            self.copy_ship()
            self.undraw_ship()
            self.draw_ship(0)
        elif button_id == "module":
            self.change_menu1("module")
            self.clean_place()
            self.copy_ship()
            self.undraw_ship()
            self.edit_mode = "module_nothing"
            self.change_menu2("module selector")
            self.draw_ship(2)
        elif button_id == "storage":
            self.edit_mode = "storage"
            self.change_menu1("storage")
            self.change_menu2("storagedefault")
            self.clean_place()
            self.copy_ship()
            self.undraw_ship()
            self.draw_ship(3)
        elif button_id == "sto_mat":
            self.edit_mode = "storage_mat"
            self.menu1.update_element("sto_mat","status",1)
            self.menu1.update_element("sto_mod","status",0)
            self.change_menu2("storage selector")
        elif button_id == "sto_mod":
            self.edit_mode = "storage_mod"
            self.menu1.update_element("sto_mat","status",0)
            self.menu1.update_element("sto_mod","status",1)
            self.change_menu2("storage module")
        elif button_id == "cancel_to_main" or button_id == "save_to_main":
            self.change_menu1("main")
            self.undraw_ship()
            if button_id == "cancel_to_main":
                self.ship_c = self.ship_l.copy()
            if button_id == "save_to_main":
                self.ship_l = self.ship_c.copy()
            self.change_menu2("default")
            self.draw_ship(1)
            self.edit_mode = "none"
            self.clean_place()
        elif button_id == "str_add_line":
            self.menu1.update_element("str_rm_line","status",0)
            self.menu1.update_element("str_add_line","status",1)
            self.edit_mode = "str_add_line"
            self.clean_place()
            self.change_menu2("material selector")
        elif button_id == "str_rm_line":
            self.change_menu2("default")
            self.menu1.update_element("str_rm_line","status",1)
            self.menu1.update_element("str_add_line","status",0)
            self.edit_mode = "str_rm_line"
            self.clean_place()
        elif button_id.startswith("mat_sel"):
            self.selected_material = button_id.split("_")[2]
            self.change_menu2("material selector")
        elif button_id.startswith("mod_sel"):
            self.selected_material = "_".join(button_id.split("_")[2:])
            self.change_menu2("module selector")
        elif button_id == "module_rotate":
            if self.selected_module:
                ori = self.selected_module.module.prop.get("orientation",0)
                self.selected_module.module.prop["orientation"] = (ori+1)%4
                self.selected_module.nr = 1
        elif button_id == "module_remove":
            if self.selected_module:
                self.button_remove_module()
        elif button_id == "module_keys":
            if self.selected_module:
                self.change_menu2("module key selector")
        elif button_id.startswith("mod_keyr_"):
            if self.selected_module:
                self.selected_key = None
                keyname = "_".join(button_id.split("_")[2:])
                mod = self.selected_module.module
                sk = [x for x in mod.keys if mod.keys[x] == keyname]
                if sk:
                    del self.selected_module.module.keys[sk[0]]
                self.change_menu2("module key selector")
        elif button_id.startswith("mod_keyg_"):
            if self.selected_module:
                keyname = "_".join(button_id.split("_")[2:])
                self.selected_key = keyname
                #if keyname in self.selected_module.module.keys:
                #    del self.selected_module.module.keys[keyname]
                self.change_menu2("module key selector")
        elif button_id.startswith("sto_m_"):
            k = button_id[6:]
            if self.selected_module:
                self.selected_module.status = 0
                self.selected_module.nr = 1
            self.selected_module = ([_ for _ in self.layer0_obj if "module" in dir(_) and _.module.name == k]+[None])[0]
            self.selected_module.status = 1
            self.selected_module.nr = 1
            self.change_menu2("storage selector")
        elif button_id.startswith("sto_o_"):
            # drop the stored module 
            k = button_id[6:]
            remmod = self.ship_c.remove_stored_module(k)
            if self.storage_free is None:
                self.storage_free = {}
            if "modules" not in self.storage_free:
                self.storage_free["modules"] = []
            self.storage_free["modules"].append(remmod)
            self.change_menu2("storage module")
        elif button_id.startswith("sto_p_"):
            # add the free module 
            k = button_id[6:]
            containername = k.split("|")[0]
            freemodname = k.split("|")[1]
            self.add_module(containername,freemodname)
            self.change_menu2("storage module")
        elif button_id.startswith("ns_storage"):
            self.storage_modify(button_id)
            if self.edit_mode == "storage_mat":
                self.change_menu2("storage selector")
            if self.edit_mode == "storage_mod":
                self.change_menu2("storage module")
        else:
            print("todo",button_id)

    def add_module(self,containername,freemodname):
        if "modules" not in self.ship_c.content.keys():
            self.ship_c.content["modules"] = []
        freemod = [_ for _ in self.storage_free["modules"] if _.name == freemodname][0]
        self.ship_c.content["modules"].append([containername,freemod])
        self.storage_free["modules"] = [_ for _ in self.storage_free["modules"] if _.name != freemodname]

    def storage_modify(self,button_id):
        el = [_ for _ in self.menu2.elements_obj if _.name == button_id][0]
        delta = el.param.get("delta",0)
        material = button_id.split(",")[2]
        module_name = button_id.split(",")[1]
        if delta < 0:
            if self.storage_free is None:
                self.storage_free = {}
            initial = self.storage_free.get(material,0)
            value = -1*delta
            self.storage_free[material] = initial+value
            self.ship_c.remove_material(module_name,material,value)
        if delta > 0:
            value = delta
            initial = self.storage_free.get(material,0)
            self.storage_free[material] = initial-value
            if self.storage_free[material] <= 0:
                del self.storage_free[material]
            self.ship_c.add_content(module_name,material,value)

    def button_remove_module(self):
        mo = self.selected_module.module
        if mo.what == "core":
            self.message.update_element("message","text","#RModule not removed. Reason: you cannot remove the core module")
            return
        ok = ""
        ship_tmp = self.ship_c.copy()
        content = {}
        if mo.what == "tank":
            content = ship_tmp.get_module_content(mo.name)
        del ship_tmp.modules[mo.name]
        r = ship_tmp.add_content("!all","modules",mo)
        if r != "success":
            ok = r
        if ok == "":
            for k in content:
                if k == "modules":
                    for mm in content["modules"]:
                        r = ship_tmp.add_content("!all",k,content[k])
                        if r != "success":
                            ok = r
                            break
                    if ok != "":
                        break
                else:
                    r = ship_tmp.add_content("!all",k,content[k])
                    if r != "success":
                        ok = r
                        break
        if ok != "":
            self.message.update_element("message","text","#RModule not removed. Reason: %s"%ok)
            return
        self.ship_c = ship_tmp.copy()
        self.clean_place()
        self.undraw_ship()
        self.edit_mode = "module_nothing"
        self.change_menu2("module selector")
        self.draw_ship(2)

    def copy_ship(self):
        self.ship_l = self.ship_c.copy()

    def clean_place(self):
        if self.selected_segment:
            if self.selected_segment in self.layer0_obj:
                self.layer0_obj.remove(self.selected_segment)
            self.selected_segment.kill()
            self.selected_segment = None
        if self.selected_node:
            if self.selected_node in self.layer0_obj:
                self.layer0_obj.remove(self.selected_node)
            self.selected_node.kill()
            self.selected_node = None
        self.selected_material = None
        self.selected_module = None
        self.selected_key = None

    def undraw_ship(self):
        for o in self.layer0_obj:
            o.kill()
        self.layer0_obj = []
        self.selected_segment = None
        self.selected_node = None

    def draw_ship(self,show_module=0):
        ship = self.ship_c
        for seg in ship.shape.segments:
            o = Ush_Segment(self,seg)
            if show_module in [2,3]:
                o.transparency = 50
            self.layer0_obj.append(o)
        for modn in ship.modules:
            mod = ship.modules[modn]
            o = Ush_Module(self,mod)
            if show_module == 0:
                o.transparency = 50
            if show_module == 3 and o.module.what not in ["core","tank"]:
                o.transparency = 50
            if show_module == 3:
                o.indic = ["left","right"]
            self.layer0_obj.append(o)

    def set_background(self):
        if self.bkgd:
            return
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        self.bkgd.fill((20,20,20))

        b = self.display.images["hangar_bkgd_white"]
        for x in range(0, rw, b.get_width()):
            for y in range(0, rh, b.get_height()):
                self.bkgd.blit(b, (x, y))

        bx,by = self.border,self.border

        x,y = self.position[0],self.position[1]
        dx,dy = x-int(x),y-int(y)
        dd = int(round(32*self.scale))

        for i in range(1+max(rh,rw)//(2*dd)):
            px = bx+dx+(i*dd*2)
            py = bx+dx+(i*dd*2)
            st = by
            while st < rh:
                pygame.draw.line(self.bkgd, (170,220,225,10), [px, st],[px, st+6])
                st += 10
            st = bx
            while st < rw:
                pygame.draw.line(self.bkgd, (170,220,225,10), [st, py],[st+6, py])
                st += 10

    def check_key(self,key):
        if self.selected_key and self.selected_module:
            mod = self.selected_module.module
            sk = [x for x in mod.keys if mod.keys[x] == self.selected_key]
            for skk in sk:
                del self.selected_module.module.keys[skk]
            self.selected_module.module.keys[pygame.key.name(key)] = self.selected_key
            self.selected_key = None
            self.change_menu2("module key selector")


    def click_rm_line(self,sx,sy):
        if self.ship_c.shape.segments == []:
            return
        seg = self.ship_c.shape.closest_segment([sx,sy])
        dist = seg.closest_approach([sx,sy])
        if dist < 0.5:
            material = ""
            for prop in seg.prop.split(","):
                if prop.startswith("M:"):
                    material = prop[2:]
                    break
            r = self.ship_c.add_content("core",material,seg.length)
            if r != "success":
                self.message.update_element("message","text","#RLine not removed. Reason: %s"%(r))
                return
            self.ship_c.shape.segments.remove(seg)
            self.ship_c.shape.update()
            self.message.update_element("message","text","Removed line, retrieved %i of %s"%(int(seg.length),material))
            self.change_menu2("default")
            if self.selected_segment:
                self.layer0_obj.remove(self.selected_segment)
                self.selected_segment.kill()
                self.selected_segment = None
            self.undraw_ship()
            self.draw_ship(0)
    
    def click_add_line(self,sx,sy):
        if self.selected_segment == None:
            cx = 2*int(round(sx/2))
            cy = 2*int(round(sy/2))
            if not self.selected_node:
                self.move_add_line(sx,sy)
            seg = Usg_Segment(self.selected_node.node[:]+[cx,cy])
            o = Ush_Segment(self,seg)
            o.status = 1
            self.selected_segment = o
            self.layer0_obj.append(o)
        else:
            q = self.selected_segment.segment.length
            if self.ship_c.has_enough_material(self.selected_material,q) == 0:
                self.message.update_element("message","text","#RLine not added. Reason: not enough material (require %i)"%int(q))
            else:
                self.ship_c.consume_material(self.selected_material,q)
                self.selected_segment.segment.prop = "M:%s"%self.selected_material
                a = self.ship_c.shape.add(self.selected_segment.segment)
                if a not in ["success","extended"]:
                    self.message.update_element("message","text","#RLine not added. Reason: %s"%a)
                else:
                    self.message.update_element("message","text","Added line, using %i of %s"%(int(q),self.selected_material))
                    self.ship_c.shape.update()
                    self.change_menu2("material selector")
            self.layer0_obj.remove(self.selected_segment)
            self.selected_segment.kill()
            self.selected_segment = None
            self.selected_node = None
            self.undraw_ship()
            self.draw_ship(0)

    def click_module(self,sx,sy):
        cx = 1*int(round(sx/1))
        cy = 1*int(round(sy/1))
        #1) did we click on an existing module
        selected = None
        moo = [x for x in self.layer0_obj if x.__class__.__name__ == "Ush_Module"]
        for mon in moo:
            mo = mon.module
            if mo.pos[0]==cx and mo.pos[1]==cy:
                selected = mon
                break
        #2) if not, place the selected module
        if selected == None:
            if self.selected_module:
                self.selected_module.module.pos = [cx,cy]
                self.selected_module.nr = 1
            elif self.edit_mode == "module_nothing":
                modules = []
                for m in self.ship_c.modules:
                    modules += self.ship_c.get_module_content(m,["modules"]).get("modules",[])
                sel_module = [x for x in modules if x.name == self.selected_material]
                if sel_module:
                    sel_module[0].pos = [cx,cy]
                    self.ship_c.modules[sel_module[0].name] = sel_module[0]
                    self.ship_c.content["modules"] = [x for x in self.ship_c.content["modules"] if x[1].name != self.selected_material]
                    self.clean_place()
                    self.undraw_ship()
                    self.selected_material = None
                    self.edit_mode = "module_nothing"
                    self.change_menu2("module selector")
                    self.draw_ship(2)
        else:
            if self.selected_module and self.selected_module.module.name == selected.module.name:
                self.selected_module.status = 0
                self.selected_module.nr = 1
                self.selected_module = None
                self.change_menu1("module")
                self.edit_mode = "module_nothing"
                self.change_menu2("module selector")
            else:
                #3) display info and add buttons
                for i in moo:
                    i.status = 0
                    i.nr = 1
                selected.status = 1
                selected.nr = 1
                self.selected_module = selected
                self.change_menu2("module description")
                self.change_menu1("module options")

    def click_storage(self,sx,sy):
        cx = 1*int(round(sx/1))
        cy = 1*int(round(sy/1))
        #1) did we click on an existing module
        selected = None
        moo = [x for x in self.layer0_obj if x.__class__.__name__ == "Ush_Module"]
        for mon in moo:
            mo = mon.module
            if mo.pos[0]==cx and mo.pos[1]==cy:
                selected = mon
                break
        #2) if not, place the selected module
        if selected is not None:
            if self.selected_module and self.selected_module.module.name == selected.module.name:
                self.selected_module.status = 0
                self.selected_module.nr = 1
                self.selected_module = None
                if self.edit_mode == "storage_mat":
                    self.change_menu2("storage selector")
                if self.edit_mode == "storage_mod":
                    self.change_menu2("storage module")
            else:
                #3) display info and add buttons
                for i in moo:
                    i.status = 0
                    i.nr = 1
                selected.status = 1
                selected.nr = 1
                self.selected_module = selected
                if self.edit_mode == "storage_mat":
                    self.change_menu2("storage selector")
                if self.edit_mode == "storage_mod":
                    self.change_menu2("storage module")

    def move_rm_line(self,sx,sy):
        shape = self.ship_c.shape
        if shape.segments == []:
            return
        seg = shape.closest_segment([sx,sy])
        dist = seg.closest_approach([sx,sy])
        redraw = 0
        if self.selected_segment == None:
            redraw = 1
        elif seg != self.selected_segment.segment:
            redraw = 1
        elif dist < 0.5 and self.selected_segment.status == 1:
            redraw = 1
        elif dist > 0.5 and self.selected_segment.status == 2:
            redraw = 1
        if redraw:
            if self.selected_segment:
                self.layer0_obj.remove(self.selected_segment)
                self.selected_segment.kill()
            o = Ush_Segment(self,seg)
            o.status = 1
            if dist < 0.5:
                o.status = 2
            self.selected_segment = o
            self.layer0_obj.append(o)
            self.change_menu2("line description")

    def move_add_line(self,sx,sy):
        cx = 2*int(round(sx/2))
        cy = 2*int(round(sy/2))
        if self.selected_node:
            self.layer0_obj.remove(self.selected_node)
            self.selected_node.kill()                
        o = Ush_Node(self,[cx,cy])
        self.layer0_obj.append(o)
        self.selected_node = o
        if self.selected_segment:
            p1 = self.selected_segment.segment.p1[:]
            if self.selected_segment in self.layer0_obj:
                self.layer0_obj.remove(self.selected_segment)
            self.selected_segment.kill()
            o = Ush_Segment(self,Usg_Segment(p1+[cx,cy]))
            o.status = 1
            self.selected_segment = o
            self.layer0_obj.append(o)

    def move_module(self,sx,sy):
        cx = 1*int(round(sx/1))
        cy = 1*int(round(sy/1))
        if self.selected_node:
            self.layer0_obj.remove(self.selected_node)
            self.selected_node.kill()                
        o = Ush_Node(self,[cx,cy])
        self.layer0_obj.append(o)
        self.selected_node = o

    def move_storage(self,sx,sy):
        cx = 1*int(round(sx/1))
        cy = 1*int(round(sy/1))
        if self.selected_node:
            self.layer0_obj.remove(self.selected_node)
            self.selected_node.kill()                
        o = Ush_Node(self,[cx,cy])
        self.layer0_obj.append(o)
        self.selected_node = o

    #def move_rm_module(self,sx,sy):
    #    d,i = [],0
    #    if not self.ship_c.modules:
    #        return
    #    for mon in self.ship_c.modules:
    #        mo = self.ship_c.modules[mon]
    #        dist = math.sqrt((mo.pos[0]-sx)**2+(mo.pos[1]-sy)**2)
    #        i += 1
    #        d.append([dist,i,mo])
    #    d.sort()
    #    redraw = 0
    #    if self.selected_node == None:
    #        redraw = 1
    #    elif self.selected_node.node[:] != d[0][2].pos[:]:
    #        redraw = 1
    #    if redraw:
    #        if self.selected_node:
    #            self.layer0_obj.remove(self.selected_node)
    #            self.selected_node.kill()                
    #        o = Ush_Node(self,d[0][2].pos[:])
    #        self.layer0_obj.append(o)
    #        self.selected_node = o
    #        self.change_menu2("module description",d[0][2])

    def drag_move(self,x,y):
        self.position[0] += 2*x
        self.position[1] += 2*y
        for o in self.layer0_obj:
            o.nr = 1
            o.refresh()

    def drag_wheel(self,d):
        rescale = 0
        if d > 0 and self.scale >= 0.125:
            rescale = 0.5
        if d < 0 and self.scale < 1:
            rescale = 2
        if rescale:
            self.scale *= rescale
            self.position[0] = int(self.position[0]/(2*rescale))*2
            self.position[1] = int(self.position[1]/(2*rescale))*2
            self.bkgd = None
            self.bkgd_first = 0
            self.drag_move(0,0)

    def mousebuttonup(self,event):
        mx,my = event.pos[0],event.pos[1]
        scale = self.scale*32
        sx = (mx-self.border+self.position[0]*scale)/scale
        sy = (my-self.border+self.position[1]*scale)/scale
        if self.edit_mode == "str_rm_line":
            self.click_rm_line(sx,sy)
        elif self.edit_mode == "str_add_line":
            if event.button==3 and self.selected_segment:
                if self.selected_segment in self.layer0_obj:
                    self.layer0_obj.remove(self.selected_segment)
                self.selected_segment.kill()
                self.selected_segment = None
            else:
                self.click_add_line(sx,sy)
        elif self.edit_mode.startswith("module"):
            self.click_module(sx,sy)
        elif self.edit_mode.startswith("storage"):
            self.click_storage(sx,sy)
        return 0

    def mousemotion(self,event):
        mx,my = event.pos[0],event.pos[1]
        scale = self.scale*32

        if event.buttons[0] == 1:
            if self.button_drag:
                dx = self.button_drag[0]-mx
                dy = self.button_drag[1]-my
                if abs(dx) > 2*scale:
                    self.drag_move(int(0.5*dx/scale),0)
                    self.button_drag[0] = mx
                if abs(dy) > 2*scale:
                    self.drag_move(0,int(0.5*dy/scale))
                    self.button_drag[1] = my
            else:
                self.button_drag = [mx,my]
        else:
            self.button_drag = None

        sx = (mx-self.border+self.position[0]*scale)/scale
        sy = (my-self.border+self.position[1]*scale)/scale
        if self.edit_mode == "str_add_line":
            self.move_add_line(sx,sy)
        elif self.edit_mode == "str_rm_line":
            self.move_rm_line(sx,sy)
        elif self.edit_mode.startswith("module"):
            self.move_module(sx,sy)
        elif self.edit_mode.startswith("storage"):
            self.move_storage(sx,sy)
        return 0

    def check_events(self,event):
        if not self.mouseevent_object:
            self.mouseevent_object = [self.menu1,self.menu2,self]

        if event.type == ACTIVEEVENT:
            ##turn on pause if the window is losing focus
            #if self.core.config.pausewhenout:
            #    if event.state == 2 and event.gain == 0:
            #        self.mode = "pause"
            pass
        elif event.type == KEYDOWN:
            key = event.key
        elif event.type == MOUSEMOTION:
            for meo in self.mouseevent_object[:]:
                block = meo.mousemotion(event)
                if block:
                    self.mouseevent_object.remove(meo)
                    self.mouseevent_object = [meo]+self.mouseevent_object
                    break
        elif event.type == MOUSEBUTTONUP and event.button < 4:
            for meo in self.mouseevent_object[:]:
                block = meo.mousebuttonup(event)
                if block:
                    break
        elif event.type == MOUSEBUTTONUP and event.button >= 4:
            self.drag_wheel((event.button*2)-9)
            

    def clear_callback(self,surf,rect):
        color = (20, 20, 20)
        surf.fill(color, rect)

    def update(self):
        dirty = []

        if self.bkgd == None:
            self.set_background()
            self.display.screen.blit(self.bkgd, (0,0))

        list_obj = [self.layer0_obj,self.layer1_obj,self.layer2_obj]
        list_spr = [self.layer0_sprite,self.layer1_sprite,self.layer2_sprite]
        redraw = [0,0,0]
        tokill = []

        for i in range(len(list_obj)):
            for o in list_obj[i][:]:
                r = o.refresh()
                if r==1:
                    redraw[i] = 1
                if r==-1:
                    tokill.append(o)
                    list_obj[i].remove(o)

        for i in range(len(list_obj)):
            if list_spr[i]:
                list_spr[i].clear(self.display.screen, self.bkgd)
        for i in range(len(list_obj)):
            if list_spr[i]:
                dirty.append(list_spr[i].draw(self.display.screen))

        for o in tokill:
            o.kill()

        redraw = 0

        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def end(self):
        for o in self.layer0_obj:
            o.kill()
        for o in self.layer1_obj:
            o.kill()
        for o in self.layer2_obj:
            o.kill()

        self.bkgd = None
        self.bkgd_first = 0

        self.time = [None,0]

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None

        self.edit_mode = "none"

        self.position = [-10,-10]
        self.scale = 1

        self.ship_o = None #original
        self.ship_l = None #last save
        self.ship_c = None #current

        self.menu1 = None
        self.menu2 = None
        self.message = None

        self.storage_free = None

        self.selected_segment = None
        self.selected_node = None
        self.selected_material = ""
        self.selected_module = None
        self.selected_key = None

        self.button_drag = None
        self.mouseevent_object = None


